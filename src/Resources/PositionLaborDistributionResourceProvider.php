<?php

namespace MiamiOH\WSPositionBudget\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;
use MiamiOH\WSPositionBudget\Services\PositionLaborDistribution\Service;

class PositionLaborDistributionResourceProvider extends ResourceProvider
{
    private $name = 'positionLaborDistribution';
    private $application = 'WebServices';
    private $module = 'Employee';

    public function registerDefinitions(): void
    {
        $this->addDefinition([
            'name' => $this->name . '.Get.model',
            'type' => 'object',
            'properties' => [
                'fiscalYearCode' => ['type' => 'string', 'enum' => ['string']],
                'percentage' => ['type' => 'string', 'enum' => ['number']],
                'position' => ['type' => 'string', 'enum' => ['string']],
                'accountCode' => ['type' => 'string', 'enum' => ['string']],
                'accountIndexCode' => ['type' => 'string', 'enum' => ['string']],
                'accountIndexCodeDescription' => ['type' => 'string', 'enum' => ['string']],
                'activityCode' => ['type' => 'string', 'enum' => ['string']],
                'budget' => ['type' => 'string', 'enum' => ['number']],
                'budgetIdCode' => ['type' => 'string', 'enum' => ['string']],
                'budgetPhaseCode' => ['type' => 'string', 'enum' => ['string']],
                'budgetToPost' => ['type' => 'string', 'enum' => ['number']],
                'changeIndicator' => ['type' => 'string', 'enum' => ['string']],
                'chartOfAccountsCode' => ['type' => 'string', 'enum' => ['string']],
                'costTypeCode' => ['type' => 'string', 'enum' => ['string']],
                'externalAccountCode' => ['type' => 'string', 'enum' => ['string']],
                'fundCode' => ['type' => 'string', 'enum' => ['string']],
                'locationCode' => ['type' => 'string', 'enum' => ['string']],
                'organizationCode' => ['type' => 'string', 'enum' => ['string']],
                'programCode' => ['type' => 'string', 'enum' => ['string']],
                'projectCode' => ['type' => 'string', 'enum' => ['string']],
                'surrogateId' => ['type' => 'string', 'enum' => ['number']],
            ]
        ]);

        $this->addDefinition([
            'name' => $this->name . '.post.model',
            'type' => 'object',
            'properties' => [
                //required fields
                'fiscalYearCode' => ['type' => 'integer', 'enum' => ['required|string']],
                'percentage' => ['type' => 'integer', 'enum' => ['required|number']],
                'position' => ['type' => 'string', 'enum' => ['required|string']],

                //non required fields
                'accountCode' => ['type' => 'string', 'enum' => ['string']],
                'accountIndexCode' => ['type' => 'string', 'enum' => ['string']],
                'activityCode' => ['type' => 'string', 'enum' => ['string']],
                'budget' => ['type' => 'string', 'enum' => ['number']],
                'budgetIdCode' => ['type' => 'string', 'enum' => ['string']],
                'budgetPhaseCode' => ['type' => 'string', 'enum' => ['string']],
                'budgetToPost' => ['type' => 'string', 'enum' => ['number']],
                'changeIndicator' => ['type' => 'string', 'enum' => ['string']],
                'chartOfAccountsCode' => ['type' => 'string', 'enum' => ['string']],
                'costTypeCode' => ['type' => 'string', 'enum' => ['string']],
                'externalAccountCode' => ['type' => 'string', 'enum' => ['string']],
                'fundCode' => ['type' => 'string', 'enum' => ['string']],
                'locationCode' => ['type' => 'string', 'enum' => ['string']],
                'organizationCode' => ['type' => 'string', 'enum' => ['string']],
                'programCode' => ['type' => 'string', 'enum' => ['string']],
                'projectCode' => ['type' => 'string', 'enum' => ['string']],
            ]
        ]);

        $this->addDefinition([
            'name' => $this->name . '.put.model',
            'type' => 'object',
            'properties' => [
                'fiscalYearCode' => ['type' => 'string', 'enum' => ['string']],
                'percentage' => ['type' => 'integer', 'enum' => ['number']],
                'position' => ['type' => 'string', 'enum' => ['string']],
                'accountCode' => ['type' => 'string', 'enum' => ['string']],
                'accountIndexCode' => ['type' => 'string', 'enum' => ['string']],
                'activityCode' => ['type' => 'string', 'enum' => ['string']],
                'budget' => ['type' => 'string', 'enum' => ['number']],
                'budgetIdCode' => ['type' => 'string', 'enum' => ['string']],
                'budgetPhaseCode' => ['type' => 'string', 'enum' => ['string']],
                'budgetToPost' => ['type' => 'string', 'enum' => ['number']],
                'changeIndicator' => ['type' => 'string', 'enum' => ['string']],
                'chartOfAccountsCode' => ['type' => 'string', 'enum' => ['string']],
                'costTypeCode' => ['type' => 'string', 'enum' => ['string']],
                'externalAccountCode' => ['type' => 'string', 'enum' => ['string']],
                'fundCode' => ['type' => 'string', 'enum' => ['string']],
                'locationCode' => ['type' => 'string', 'enum' => ['string']],
                'organizationCode' => ['type' => 'string', 'enum' => ['string']],
                'programCode' => ['type' => 'string', 'enum' => ['string']],
                'projectCode' => ['type' => 'string', 'enum' => ['string']],
            ]
        ]);

        $this->addDefinition([
            'name' => $this->name . '.post.model.v2',
            'type' => 'object',
            'properties' => [
                //required fields
                'percentage' => ['type' => 'integer', 'enum' => ['required|number']],
                'accountCode' => ['type' => 'string', 'enum' => ['required|string']],
                'accountIndexCode' => ['type' => 'string', 'enum' => ['required|tring']],

                //non required fields
                'budget' => ['type' => 'string', 'enum' => ['number']],
                'budgetToPost' => ['type' => 'string', 'enum' => ['number']],
                'changeIndicator' => ['type' => 'string', 'enum' => ['string']],
                'chartOfAccountsCode' => ['type' => 'string', 'enum' => ['string']],
                'costTypeCode' => ['type' => 'string', 'enum' => ['string']],
                'externalAccountCode' => ['type' => 'string', 'enum' => ['string']],
                'projectCode' => ['type' => 'string', 'enum' => ['string']],
                'userId' => ['type' => 'string', 'enum' => ['string']],
                'budgetIdCode' => ['type' => 'string', 'enum' => ['string']],
                'budgetPhaseCode' => ['type' => 'string', 'enum' => ['string']],
            ]
        ]);

        $this->addDefinition([
            'name' => $this->name . '.put.model.v2',
            'type' => 'object',
            'properties' => [
                //required fields
                'percentage' => ['type' => 'integer', 'enum' => ['required|number']],
                'accountCode' => ['type' => 'string', 'enum' => ['required|string']],
                'accountIndexCode' => ['type' => 'string', 'enum' => ['required|tring']],

                //non required fields
                'budget' => ['type' => 'string', 'enum' => ['number']],
                'budgetToPost' => ['type' => 'string', 'enum' => ['number']],
                'changeIndicator' => ['type' => 'string', 'enum' => ['string']],
                'chartOfAccountsCode' => ['type' => 'string', 'enum' => ['string']],
                'costTypeCode' => ['type' => 'string', 'enum' => ['string']],
                'externalAccountCode' => ['type' => 'string', 'enum' => ['string']],
                'projectCode' => ['type' => 'string', 'enum' => ['string']],
                'userId' => ['type' => 'string', 'enum' => ['string']],
                'budgetIdCode' => ['type' => 'string', 'enum' => ['string']],
                'budgetPhaseCode' => ['type' => 'string', 'enum' => ['string']],
            ]
        ]);

        $this->addDefinition([
            'name' => $this->name . '.post.collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/' . $this->name . '.post.model.v2'
            ]
        ]);


        $this->addDefinition([
            'name' => $this->name . '.put.collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/' . $this->name . '.put.model.v2'
            ]
        ]);
    }

    public function registerServices(): void
    {
        $this->addService([
            'name' => $this->name,
            'class' => Service::class,
            'description' => 'Provide database source',
//            'set' => [
//                'dataSource' => ['type' => 'service', 'name' => 'APIDataSource']
//            ]
        ]);
    }

    public function registerResources(): void
    {
        $this->addResource([
            'action' => 'read',
            'description' => 'Get position labor distribution data',
            'name' => 'PositionLaborDistribution.read.collection',
            'service' => $this->name,
            'method' => 'getPositionLaborDistribution',
            'returnType' => 'collection',
            'tags' => ['position'],
            'pattern' => '/position/laborDistribution/v1',
            'isPartiable' => true,
            'options' => [
                'position' => [
                    'type' => 'single',
                    'description' => 'Return the labor distribution records for corresponding position'
                ],
                'fiscalYearCode' => [
                    'type' => 'single',
                    'description' => 'Return the labor distribution records for corresponding fiscal year'
                ],
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'A collection of employee identification records',
                    'returns' => [
                        'type' => 'array',
                        '$ref' => '#/definitions/' . $this->name . '.Get.model',
                    ]
                ],
                App::API_NOTFOUND => [
                    'description' => 'There is no record found.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => $this->application,
                    'module' => $this->module,
                    'key' => ['view', 'all']
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'create',
            'description' => 'Create position labor distribution',
            'name' => $this->name . '.post.single',
            'service' => $this->name,
            'method' => 'postSingle',
            'tags' => ['position'],
            'pattern' => '/position/laborDistribution/v1',
            'body' => [
                'required' => true,
                'description' => 'Position Labor Distribution data',
                'schema' => [
                    '$ref' => '#/definitions/' . $this->name . '.post.model'
                ]
            ],
            'responses' => [
                App::API_CREATED => [
                    'description' => 'Creates or updates successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Insert operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => $this->application,
                    'module' => $this->module,
                    'key' => ['create', 'all']
                ],
            ]
        ]);


        $this->addResource([
            'action' => 'create',
            'description' => 'Create position labor distribution ',
            'name' => $this->name . '.post.collection',
            'service' => $this->name,
            'method' => 'postCollection',
            'tags' => ['position'],
            'pattern' => '/position/laborDistribution/v2/:positionNumber/:fiscalYear',
            'params' => [
                'positionNumber' => ['description' => 'Position Number for the resource that is being created'],
                'fiscalYear' => ['description' => 'Fiscal Year for the resource that is being created'],
            ],
            'body' => [
                'required' => true,
                'description' => 'Collection of Position Labor Distribution data',
                'schema' => [
                    '$ref' => '#/definitions/' . $this->name . '.post.collection'
                ]
            ],
            'responses' => [
                App::API_CREATED => [
                    'description' => 'Creates or updates successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Insert operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => $this->application,
                    'module' => $this->module,
                    'key' => ['create', 'all']
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'update',
            'description' => 'Update position labor distribution data',
            'name' => $this->name . '.put.single',
            'service' => $this->name,
            'method' => 'putSingle',
            'tags' => ['position'],
            'pattern' => '/position/laborDistribution/v1/:surrogateId',
            'params' => [
                'surrogateId' => ['description' => 'Surrogate ID for the record that is being updated'],
            ],
            'body' => [
                'required' => true,
                'description' => 'Position Labor Distribution data',
                'schema' => [
                    '$ref' => '#/definitions/' . $this->name . '.put.model'
                ]
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'Record Updated successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_NOTFOUND => [
                    'description' => 'Record not found.',
                ],
                App::API_FAILED => [
                    'description' => 'Update operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],

            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => $this->application,
                    'module' => $this->module,
                    'key' => ['update', 'all']
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'update',
            'description' => 'Update exiting position labor distribution to inactive and creating new position labor distribution records',
            'name' => $this->name . '.put.collection',
            'service' => $this->name,
            'method' => 'putCollection',
            'tags' => ['position'],
            'pattern' => '/position/laborDistribution/v2/:positionNumber/:fiscalYear',
            'params' => [
                'positionNumber' => ['description' => 'Position Number for the resource that is being updated'],
                'fiscalYear' => ['description' => 'Fiscal Year for the resource that is being updated'],
            ],
            'body' => [
                'required' => true,
                'description' => 'Collection of Position Labor Distribution data',
                'schema' => [
                    '$ref' => '#/definitions/' . $this->name . '.put.collection'
                ]
            ],
            'responses' => [
                App::API_CREATED => [
                    'description' => 'Creates or updates successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Insert operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => $this->application,
                    'module' => $this->module,
                    'key' => ['update', 'all']
                ],
            ]
        ]);
    }

    public function registerOrmConnections(): void
    {
    }
}
