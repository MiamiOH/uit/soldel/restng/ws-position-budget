<?php

namespace MiamiOH\WSPositionBudget\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;
use MiamiOH\WSPositionBudget\Services\JobLaborDistribution\Service;

class JobLaborDistributionResourceProvider extends ResourceProvider
{
    private $name = 'jobLaborDistribution';
    private $application = 'WebServices';
    private $module = 'Employee';

    public function registerDefinitions(): void
    {
        $this->addDefinition([
            'name' => $this->name . '.Get.model',
            'type' => 'object',
            'properties' => [
                'muid' => ['type' => 'string', 'enum' => ['string']],
                'position' => ['type' => 'string', 'enum' => ['string']],
                'suffix' => ['type' => 'string', 'enum' => ['string']],
                'effectiveDate' => ['type' => 'string', 'enum' => ['string']],
                'chartOfAccountsCode' => ['type' => 'string', 'enum' => ['string']],
                'accountIndexCode' => ['type' => 'string', 'enum' => ['string']],
                'fundCode' => ['type' => 'string', 'enum' => ['string']],
                'organizationCode' => ['type' => 'string', 'enum' => ['string']],
                'accountCode' => ['type' => 'string', 'enum' => ['string']],
                'programCode' => ['type' => 'string', 'enum' => ['string']],
                'activityCode' => ['type' => 'string', 'enum' => ['string']],
                'locationCode' => ['type' => 'string', 'enum' => ['string']],
                'projectCode' => ['type' => 'string', 'enum' => ['string']],
                'costTypeCode' => ['type' => 'string', 'enum' => ['string']],
                'externalAccountCode' => ['type' => 'string', 'enum' => ['string']],
                'percentage' => ['type' => 'string', 'enum' => ['number']],
                'encumbranceNumber' => ['type' => 'string', 'enum' => ['string']],
                'encumbranceSequenceNumber' => ['type' => 'string', 'enum' => ['number']],
                'salaryEncumbrance' => ['type' => 'string', 'enum' => ['number']],
                'salaryEncumbranceToPost' => ['type' => 'string', 'enum' => ['number']],
                'fringeEncumbrance' => ['type' => 'string', 'enum' => ['number']],
                'fringeEncumbranceToPost' => ['type' => 'string', 'enum' => ['number']],
                'changeIndicator' => ['type' => 'string', 'enum' => ['string']],
                'fringeFund' => ['type' => 'string', 'enum' => ['string']],
                'fringeOrganization' => ['type' => 'string', 'enum' => ['string']],
                'fringeAccount' => ['type' => 'string', 'enum' => ['string']],
                'fringeProgram' => ['type' => 'string', 'enum' => ['string']],
                'fringeActivity' => ['type' => 'string', 'enum' => ['string']],
                'fringeLocation' => ['type' => 'string', 'enum' => ['string']],
                'fringeEncumbranceResidual' => ['type' => 'string', 'enum' => ['number']],
                'fringeEncumbranceResidualToPost' => ['type' => 'string', 'enum' => ['number']],
                'fringeEncumbranceSequenceNumber' => ['type' => 'string', 'enum' => ['number']],
                'futureFringeEncumbrance' => ['type' => 'string', 'enum' => ['number']],
                'futureFringeEncumbranceResidual' => ['type' => 'string', 'enum' => ['number']],
                'futureFringeEncumbranceResidualToPost' => ['type' => 'string', 'enum' => ['number']],
                'futureFringeEncumbranceToPost' => ['type' => 'string', 'enum' => ['number']],
                'futureSalaryEncumbrance' => ['type' => 'string', 'enum' => ['number']],
                'futureSalaryEncumbranceToPost' => ['type' => 'string', 'enum' => ['number']],
                'encumbranceLastCalculatedDate' => ['type' => 'string', 'enum' => ['date']],
                'encumbranceLatestRecastDate' => ['type' => 'string', 'enum' => ['date']],
                'encumbranceOverrideEndDate' => ['type' => 'string', 'enum' => ['date']],
                'fringeEncumbranceNumber' => ['type' => 'string', 'enum' => ['number']],
                'activityDate' => ['type' => 'string', 'enum' => ['string']],
                'userId' => ['type' => 'string', 'enum' => ['string']],
                'dataOrigin' => ['type' => 'string', 'enum' => ['string']],
                'surrogateId' => ['type' => 'string', 'enum' => ['number']],
            ]
        ]);

        $this->addDefinition([
            'name' => $this->name . '.post.model',
            'type' => 'object',
            'properties' => [
                // required fields
                'changeIndicator' => ['type' => 'string', 'enum' => ['required|string']],
                'effectiveDate' => ['type' => 'string', 'enum' => ['required|date']],
                'fringeEncumbranceToPost' => ['type' => 'string', 'enum' => ['required|number']],
                'muid' => ['type' => 'string', 'enum' => ['required|string']],
                'percentage' => ['type' => 'string', 'enum' => ['required|number']],
                'position' => ['type' => 'string', 'enum' => ['required|string']],
                'salaryEncumbranceToPost' => ['type' => 'string', 'enum' => ['required|number']],
                'suffix' => ['type' => 'string', 'enum' => ['required|string']],

                // non required fields
                'accountCode' => ['type' => 'string', 'enum' => ['string']],
                'accountIndexCode' => ['type' => 'string', 'enum' => ['string']],
                'activityCode' => ['type' => 'string', 'enum' => ['string']],
                'chartOfAccountsCode' => ['type' => 'string', 'enum' => ['string']],
                'costTypeCode' => ['type' => 'string', 'enum' => ['string']],
                'encumbranceNumber' => ['type' => 'string', 'enum' => ['string']],
                'encumbranceLastCalculatedDate' => ['type' => 'string', 'enum' => ['date']],
                'encumbranceLatestRecastDate' => ['type' => 'string', 'enum' => ['date']],
                'encumbranceOverrideEndDate' => ['type' => 'string', 'enum' => ['date']],
                'encumbranceSequenceNumber' => ['type' => 'string', 'enum' => ['number']],
                'externalAccountCode' => ['type' => 'string', 'enum' => ['string']],
                'fringeEncumbrance' => ['type' => 'string', 'enum' => ['number']],
                'fringeFund' => ['type' => 'string', 'enum' => ['string']],
                'fringeOrganization' => ['type' => 'string', 'enum' => ['string']],
                'fringeAccount' => ['type' => 'string', 'enum' => ['string']],
                'fringeProgram' => ['type' => 'string', 'enum' => ['string']],
                'fringeActivity' => ['type' => 'string', 'enum' => ['string']],
                'fringeLocation' => ['type' => 'string', 'enum' => ['string']],
                'fringeEncumbranceNumber' => ['type' => 'string', 'enum' => ['number']],
                'fringeEncumbranceResidual' => ['type' => 'string', 'enum' => ['number']],
                'fringeEncumbranceResidualToPost' => ['type' => 'string', 'enum' => ['number']],
                'fringeEncumbranceSequenceNumber' => ['type' => 'string', 'enum' => ['number']],
                'futureFringeEncumbrance' => ['type' => 'string', 'enum' => ['number']],
                'futureFringeEncumbranceResidual' => ['type' => 'string', 'enum' => ['number']],
                'futureFringeEncumbranceResidualToPost' => ['type' => 'string', 'enum' => ['number']],
                'futureFringeEncumbranceToPost' => ['type' => 'string', 'enum' => ['number']],
                'futureSalaryEncumbrance' => ['type' => 'string', 'enum' => ['number']],
                'futureSalaryEncumbranceToPost' => ['type' => 'string', 'enum' => ['number']],
                'fundCode' => ['type' => 'string', 'enum' => ['string']],
                'locationCode' => ['type' => 'string', 'enum' => ['string']],
                'organizationCode' => ['type' => 'string', 'enum' => ['string']],
                'programCode' => ['type' => 'string', 'enum' => ['string']],
                'projectCode' => ['type' => 'string', 'enum' => ['string']],
                'salaryEncumbrance' => ['type' => 'string', 'enum' => ['number']],
            ]
        ]);

        $this->addDefinition([
            'name' => $this->name . '.put.model',
            'type' => 'object',
            'properties' => [
                'changeIndicator' => ['type' => 'string', 'enum' => ['string']],
                'effectiveDate' => ['type' => 'string', 'enum' => ['date']],
                'fringeEncumbranceToPost' => ['type' => 'string', 'enum' => ['number']],
                'muid' => ['type' => 'string', 'enum' => ['string']],
                'percentage' => ['type' => 'string', 'enum' => ['number']],
                'position' => ['type' => 'string', 'enum' => ['string']],
                'salaryEncumbranceToPost' => ['type' => 'string', 'enum' => ['number']],
                'suffix' => ['type' => 'string', 'enum' => ['string']],
                'accountCode' => ['type' => 'string', 'enum' => ['string']],
                'accountIndexCode' => ['type' => 'string', 'enum' => ['string']],
                'activityCode' => ['type' => 'string', 'enum' => ['string']],
                'chartOfAccountsCode' => ['type' => 'string', 'enum' => ['string']],
                'costTypeCode' => ['type' => 'string', 'enum' => ['string']],
                'encumbranceNumber' => ['type' => 'string', 'enum' => ['string']],
                'encumbranceLastCalculatedDate' => ['type' => 'string', 'enum' => ['date']],
                'encumbranceLatestRecastDate' => ['type' => 'string', 'enum' => ['date']],
                'encumbranceOverrideEndDate' => ['type' => 'string', 'enum' => ['date']],
                'encumbranceSequenceNumber' => ['type' => 'string', 'enum' => ['number']],
                'externalAccountCode' => ['type' => 'string', 'enum' => ['string']],
                'fringeEncumbrance' => ['type' => 'string', 'enum' => ['number']],
                'fringeFund' => ['type' => 'string', 'enum' => ['string']],
                'fringeOrganization' => ['type' => 'string', 'enum' => ['string']],
                'fringeAccount' => ['type' => 'string', 'enum' => ['string']],
                'fringeProgram' => ['type' => 'string', 'enum' => ['string']],
                'fringeActivity' => ['type' => 'string', 'enum' => ['string']],
                'fringeLocation' => ['type' => 'string', 'enum' => ['string']],
                'fringeEncumbranceNumber' => ['type' => 'string', 'enum' => ['number']],
                'fringeEncumbranceResidual' => ['type' => 'string', 'enum' => ['number']],
                'fringeEncumbranceResidualToPost' => ['type' => 'string', 'enum' => ['number']],
                'fringeEncumbranceSequenceNumber' => ['type' => 'string', 'enum' => ['number']],
                'futureFringeEncumbrance' => ['type' => 'string', 'enum' => ['number']],
                'futureFringeEncumbranceResidual' => ['type' => 'string', 'enum' => ['number']],
                'futureFringeEncumbranceResidualToPost' => ['type' => 'string', 'enum' => ['number']],
                'futureFringeEncumbranceToPost' => ['type' => 'string', 'enum' => ['number']],
                'futureSalaryEncumbrance' => ['type' => 'string', 'enum' => ['number']],
                'futureSalaryEncumbranceToPost' => ['type' => 'string', 'enum' => ['number']],
                'fundCode' => ['type' => 'string', 'enum' => ['string']],
                'locationCode' => ['type' => 'string', 'enum' => ['string']],
                'organizationCode' => ['type' => 'string', 'enum' => ['string']],
                'programCode' => ['type' => 'string', 'enum' => ['string']],
                'projectCode' => ['type' => 'string', 'enum' => ['string']],
                'salaryEncumbrance' => ['type' => 'string', 'enum' => ['number']],
            ]
        ]);

        $this->addDefinition([
            'name' => $this->name . '.post.model.v2',
            'type' => 'object',
            'properties' => [
                // required fields
                'percentage' => ['type' => 'string', 'enum' => ['required|number']],
                'changeIndicator' => ['type' => 'string', 'enum' => ['required|string']],
                'salaryEncumbranceToPost' => ['type' => 'string', 'enum' => ['required|number']],
                'fringeEncumbranceToPost' => ['type' => 'string', 'enum' => ['required|number']],

                // non required fields
                'accountCode' => ['type' => 'string', 'enum' => ['string']],
                'accountIndexCode' => ['type' => 'string', 'enum' => ['string']],
                'chartOfAccountsCode' => ['type' => 'string', 'enum' => ['string']],
                'costTypeCode' => ['type' => 'string', 'enum' => ['string']],

                'encumbranceNumber' => ['type' => 'string', 'enum' => ['string']],
                'encumbranceLastCalculatedDate' => ['type' => 'string', 'enum' => ['date']],
                'encumbranceLatestRecastDate' => ['type' => 'string', 'enum' => ['date']],
                'encumbranceOverrideEndDate' => ['type' => 'string', 'enum' => ['date']],
                'encumbranceSequenceNumber' => ['type' => 'string', 'enum' => ['number']],
                'externalAccountCode' => ['type' => 'string', 'enum' => ['string']],
                'fringeEncumbrance' => ['type' => 'string', 'enum' => ['number']],
                'fringeFund' => ['type' => 'string', 'enum' => ['string']],
                'fringeOrganization' => ['type' => 'string', 'enum' => ['string']],
                'fringeAccount' => ['type' => 'string', 'enum' => ['string']],
                'fringeProgram' => ['type' => 'string', 'enum' => ['string']],
                'fringeActivity' => ['type' => 'string', 'enum' => ['string']],
                'fringeLocation' => ['type' => 'string', 'enum' => ['string']],
                'fringeEncumbranceNumber' => ['type' => 'string', 'enum' => ['number']],
                'fringeEncumbranceResidual' => ['type' => 'string', 'enum' => ['number']],
                'fringeEncumbranceResidualToPost' => ['type' => 'string', 'enum' => ['number']],
                'fringeEncumbranceSequenceNumber' => ['type' => 'string', 'enum' => ['number']],
                'futureFringeEncumbrance' => ['type' => 'string', 'enum' => ['number']],
                'futureFringeEncumbranceResidual' => ['type' => 'string', 'enum' => ['number']],
                'futureFringeEncumbranceResidualToPost' => ['type' => 'string', 'enum' => ['number']],
                'futureFringeEncumbranceToPost' => ['type' => 'string', 'enum' => ['number']],
                'futureSalaryEncumbrance' => ['type' => 'string', 'enum' => ['number']],
                'futureSalaryEncumbranceToPost' => ['type' => 'string', 'enum' => ['number']],
                'projectCode' => ['type' => 'string', 'enum' => ['string']],
                'salaryEncumbrance' => ['type' => 'string', 'enum' => ['number']]
            ]
        ]);

        $this->addDefinition([
            'name' => $this->name . '.post.collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/' . $this->name . '.post.model.v2'
            ]
        ]);

        // using same model as post
        $this->addDefinition([
            'name' => $this->name . '.put.collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/' . $this->name . '.post.model.v2'
            ]
        ]);
    }

    public function registerServices(): void
    {
        $this->addService([
            'name' => $this->name,
            'class' => Service::class,
            'description' => 'Provide database source',
        ]);
    }

    public function registerResources(): void
    {
        $this->addResource([
            'action' => 'read',
            'description' => 'Get job labor distribution data',
            'name' => 'JobLaborDistribution.read.collection',
            'service' => $this->name,
            'method' => 'getJobLaborDistribution',
            'tags' => ['job'],
            'pattern' => '/job/laborDistribution/v1',
            'isPageable' => true,
            'defaultPageLimit' => 100,
            'maxPageLimit' => 500,
            'options' => [
                'muid' => [
                    'type' => 'single',
                    'description' => 'Return the job labor distribution records for corresponding muid'
                ],
                'position' => [
                    'type' => 'single',
                    'description' => 'Return the job labor distribution records for corresponding position'
                ],
                'suffix' => [
                    'type' => 'single',
                    'description' => 'Return the job labor distribution records for corresponding suffix'
                ],
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'A collection of employee identification records',
                    'returns' => [
                        'type' => 'array',
                        '$ref' => '#/definitions/' . $this->name . '.get.model',
                    ]
                ],
                App::API_NOTFOUND => [
                    'description' => 'There is no record found.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => $this->application,
                    'module' => $this->module,
                    'key' => ['view', 'all']
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'create',
            'description' => 'Create job labor distribution collection',
            'name' => $this->name . '.post.collection',
            'service' => $this->name,
            'method' => 'postCollection',
            'tags' => ['job'],
            'pattern' => '/job/laborDistribution/v2/:muid/:positionNumber/:positionSuffix/:effectiveDate',
            'params' => [
                'muid' => ['description' => "Employee's unique ID or banner ID or PIDM"],
                'positionNumber' => ['description' => 'Position Number for the resource that is being created'],
                'positionSuffix' => ['description' => 'Position Suffix for the resource that is being created'],
                'effectiveDate' => ['description' => 'Effective date for the resource that is being created'],
            ],
            'body' => [
                'required' => true,
                'description' => 'Job Labor Distribution data',
                'schema' => [
                    '$ref' => '#/definitions/' . $this->name . '.post.collection'
                ]
            ],
            'responses' => [
                App::API_CREATED => [
                    'description' => 'Creates or updates successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_ENTITYALREADYEXISTS => [
                    'description' => 'Record cannot be created because it already exists.',
                ],
                App::API_FAILED => [
                    'description' => 'Insert operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => $this->application,
                    'module' => $this->module,
                    'key' => ['create', 'all']
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'update',
            'description' => 'Update job labor distribution collection',
            'name' => $this->name . '.put.collection',
            'service' => $this->name,
            'method' => 'putCollection',
            'tags' => ['job'],
            'pattern' => '/job/laborDistribution/v2/:muid/:positionNumber/:positionSuffix/:effectiveDate',
            'params' => [
                'muid' => ['description' => "Employee's unique ID or banner ID or PIDM"],
                'positionNumber' => ['description' => 'Position Number for the resource that is being created'],
                'positionSuffix' => ['description' => 'Position Suffix for the resource that is being created'],
                'effectiveDate' => ['description' => 'Effective date for the resource that is being created'],
            ],
            'body' => [
                'required' => true,
                'description' => 'Job Labor Distribution data',
                'schema' => [
                    '$ref' => '#/definitions/' . $this->name . '.put.collection'
                ]
            ],
            'responses' => [
                App::API_CREATED => [
                    'description' => 'Creates or updates successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_NOTFOUND => [
                    'description' => 'Records cannot be updated before it is not found.',
                ],
                App::API_FAILED => [
                    'description' => 'Insert operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => $this->application,
                    'module' => $this->module,
                    'key' => ['update', 'all']
                ],
            ]
        ]);
    }

    public function registerOrmConnections(): void
    {
    }
}
