<?php

namespace MiamiOH\WSPositionBudget\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;
use MiamiOH\WSPositionBudget\Services\PositionBudget\Service;

class PositionBudgetResourceProvider extends ResourceProvider
{
    private $name = 'positionBudget';
    private $application = 'WebServices';
    private $module = 'Employee';

    public function registerDefinitions(): void
    {
        $this->addDefinition([
            'name' => $this->name . '.get.model',
            'type' => 'object',
            'properties' => [
                'appointmentPercent' => ['type' => 'string', 'enum' => ['number']],
                'baseUnits' => ['type' => 'string', 'enum' => ['number']],
                'createNbrjfteIndicator' => ['type' => 'string', 'enum' => ['string']],
                'effectiveDate' => ['type' => 'string', 'enum' => ['date']],
                'fiscalYearCode' => ['type' => 'string', 'enum' => ['string']],
                'fullTimeEquivalency' => ['type' => 'string', 'enum' => ['number']],
                'organizationCode' => ['type' => 'string', 'enum' => ['string']],
                'position' => ['type' => 'string', 'enum' => ['string']],
                'positionAnnualBasis' => ['type' => 'string', 'enum' => ['number']],
                'positionBudgetBasis' => ['type' => 'string', 'enum' => ['number']],
                'accountIndexCode' => ['type' => 'string', 'enum' => ['string']],
                'budget' => ['type' => 'string', 'enum' => ['number']],
                'budgetIdCode' => ['type' => 'string', 'enum' => ['string']],
                'budgetPhaseCode' => ['type' => 'string', 'enum' => ['string']],
                'chartOfAccountsCode' => ['type' => 'string', 'enum' => ['string']],
                'comment' => ['type' => 'string', 'enum' => ['string']],
                'encumbranceAmount' => ['type' => 'string', 'enum' => ['number']],
                'expenditureAmount' => ['type' => 'string', 'enum' => ['number']],
                'fringeAccountCode' => ['type' => 'string', 'enum' => ['string']],
                'fringeActivityCode' => ['type' => 'string', 'enum' => ['string']],
                'fringeEncumbrance' => ['type' => 'string', 'enum' => ['number']],
                'fringeExpenditure' => ['type' => 'string', 'enum' => ['number']],
                'fringeFundCode' => ['type' => 'string', 'enum' => ['string']],
                'fringeLocationCode' => ['type' => 'string', 'enum' => ['string']],
                'fringeOrganizationCode' => ['type' => 'string', 'enum' => ['string']],
                'fringeProgramCode' => ['type' => 'string', 'enum' => ['string']],
                'positionBudgetFringe' => ['type' => 'string', 'enum' => ['number']],
                'salaryGroupCode' => ['type' => 'string', 'enum' => ['string']],
                'savingAccountPosition' => ['type' => 'string', 'enum' => ['string']],
                'savingAccountBudget' => ['type' => 'string', 'enum' => ['string']],
                'status' => ['type' => 'string', 'enum' => ['string']],
                'recurringBudgetAmount' => ['type' => 'string', 'enum' => ['number']],
            ]
        ]);

        $this->addDefinition([
            'name' => $this->name . '.post.model',
            'type' => 'object',
            'properties' => [
                // required fields
                'appointmentPercent' => ['type' => 'string', 'enum' => ['required|number']],
                'baseUnits' => ['type' => 'string', 'enum' => ['required|number']],
                'createNbrjfteIndicator' => ['type' => 'string', 'enum' => ['required|string']],
                'effectiveDate' => ['type' => 'string', 'enum' => ['required|date']],
                'fiscalYearCode' => ['type' => 'string', 'enum' => ['required|string']],
                'fullTimeEquivalency' => ['type' => 'string', 'enum' => ['required|number']],
                'organizationCode' => ['type' => 'string', 'enum' => ['required|string']],
                'position' => ['type' => 'string', 'enum' => ['required|string']],
                'positionAnnualBasis' => ['type' => 'string', 'enum' => ['required|number']],
                'positionBudgetBasis' => ['type' => 'string', 'enum' => ['required|number']],

                // non required fields
                'accountIndexCode' => ['type' => 'string', 'enum' => ['string']],
                'budget' => ['type' => 'string', 'enum' => ['number']],
                'budgetIdCode' => ['type' => 'string', 'enum' => ['string']],
                'budgetPhaseCode' => ['type' => 'string', 'enum' => ['string']],
                'chartOfAccountsCode' => ['type' => 'string', 'enum' => ['string']],
                'comment' => ['type' => 'string', 'enum' => ['string']],
                'encumbranceAmount' => ['type' => 'string', 'enum' => ['number']],
                'expenditureAmount' => ['type' => 'string', 'enum' => ['number']],
                'fringeAccountCode' => ['type' => 'string', 'enum' => ['string']],
                'fringeActivityCode' => ['type' => 'string', 'enum' => ['string']],
                'fringeEncumbrance' => ['type' => 'string', 'enum' => ['number']],
                'fringeExpenditure' => ['type' => 'string', 'enum' => ['number']],
                'fringeFundCode' => ['type' => 'string', 'enum' => ['string']],
                'fringeLocationCode' => ['type' => 'string', 'enum' => ['string']],
                'fringeOrganizationCode' => ['type' => 'string', 'enum' => ['string']],
                'fringeProgramCode' => ['type' => 'string', 'enum' => ['string']],
                'positionBudgetFringe' => ['type' => 'string', 'enum' => ['number']],
                'salaryGroupCode' => ['type' => 'string', 'enum' => ['string']],
                'status' => ['type' => 'string', 'enum' => ['string']],
                'recurringBudgetAmount' => ['type' => 'string', 'enum' => ['number']],
            ]
        ]);

        $this->addDefinition([
            'name' => $this->name . '.put.model',
            'type' => 'object',
            'properties' => [
                'appointmentPercent' => ['type' => 'string', 'enum' => ['number']],
                'baseUnits' => ['type' => 'string', 'enum' => ['number']],
                'createNbrjfteIndicator' => ['type' => 'string', 'enum' => ['string']],
                'effectiveDate' => ['type' => 'string', 'enum' => ['date']],
                'fullTimeEquivalency' => ['type' => 'string', 'enum' => ['number']],
                'organizationCode' => ['type' => 'string', 'enum' => ['string']],
                'positionAnnualBasis' => ['type' => 'string', 'enum' => ['number']],
                'positionBudgetBasis' => ['type' => 'string', 'enum' => ['number']],
                'accountIndexCode' => ['type' => 'string', 'enum' => ['string']],
                'budget' => ['type' => 'string', 'enum' => ['number']],
                'budgetIdCode' => ['type' => 'string', 'enum' => ['string']],
                'budgetPhaseCode' => ['type' => 'string', 'enum' => ['string']],
                'chartOfAccountsCode' => ['type' => 'string', 'enum' => ['string']],
                'comment' => ['type' => 'string', 'enum' => ['string']],
                'encumbranceAmount' => ['type' => 'string', 'enum' => ['number']],
                'expenditureAmount' => ['type' => 'string', 'enum' => ['number']],
                'fringeAccountCode' => ['type' => 'string', 'enum' => ['string']],
                'fringeActivityCode' => ['type' => 'string', 'enum' => ['string']],
                'fringeEncumbrance' => ['type' => 'string', 'enum' => ['number']],
                'fringeExpenditure' => ['type' => 'string', 'enum' => ['number']],
                'fringeFundCode' => ['type' => 'string', 'enum' => ['string']],
                'fringeLocationCode' => ['type' => 'string', 'enum' => ['string']],
                'fringeOrganizationCode' => ['type' => 'string', 'enum' => ['string']],
                'fringeProgramCode' => ['type' => 'string', 'enum' => ['string']],
                'positionBudgetFringe' => ['type' => 'string', 'enum' => ['number']],
                'salaryGroupCode' => ['type' => 'string', 'enum' => ['string']],
                'status' => ['type' => 'string', 'enum' => ['string']],
                'recurringBudgetAmount' => ['type' => 'string', 'enum' => ['number']],
            ]
        ]);
    }


    public function registerServices(): void
    {
        $this->addService([
            'name' => $this->name,
            'class' => Service::class,
            'description' => 'Provide database source',
//            'set' => [
//                'dataSource' => ['type' => 'service', 'name' => 'APIDataSource']
//            ]
        ]);
    }

    public function registerResources(): void
    {
        $this->addResource([
            'action' => 'read',
            'description' => 'Read position budget',
            'name' => $this->name . '.get.single',
            'service' => $this->name,
            'method' => 'getSingle',
            'isPageable' => true,
            'tags' => ['position'],
            'pattern' => '/position/budget/v1',
            'options' => [
                'positionNumber' => [
                    'type' => 'single',
                    'required' => true,
                    'description' => 'Return the budget details that match position number'
                ],
                'fiscalYear' => [
                    'type' => 'single',
                    'required' => true,
                    'description' => 'Return the budget details that match fiscal year'
                ]
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'A collection of position budget records.',
                    'returns' => [
                        'type' => 'array',
                        '$ref' => '#/definitions/' . $this->name . '.get.model',
                    ]
                ],
                App::API_NOTFOUND => [
                    'description' => 'There is no record found.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => $this->application,
                    'module' => $this->module,
                    'key' => ['view', 'all']
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'create',
            'description' => 'Create position budget',
            'name' => $this->name . '.post.single',
            'service' => $this->name,
            'method' => 'postSingle',
            'tags' => ['position'],
            'pattern' => '/position/budget/v1',
            'body' => [
                'required' => true,
                'description' => 'Position Budget data',
                'schema' => [
                    '$ref' => '#/definitions/' . $this->name . '.post.model'
                ]
            ],
            'responses' => [
                App::API_CREATED => [
                    'description' => 'Creates or updates successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Insert operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => $this->application,
                    'module' => $this->module,
                    'key' => ['create', 'all']
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'update',
            'name' => $this->name . '.put.single',
            'description' => 'Update position budget',
            'service' => $this->name,
            'method' => 'putSingle',
            'tags' => ['position'],
            'pattern' => '/position/budget/v1/:positionNumber/:fiscalYearCode',
            'returnType' => 'model',
            'body' => [
                'description' => 'Collection of Position Budget objects',
                'required' => true,
                'schema' => [
                    '$ref' => '#/definitions/' . $this->name . '.put.model'
                ]
            ],
            'params' => [
                'positionNumber' => [
                    'type' => 'single',
                    'description' => 'Return the budget details that match position number'
                ],
                'fiscalYearCode' => [
                    'type' => 'single',
                    'description' => 'Return the budget details that match year Code'
                ],
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'Record Updated successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_NOTFOUND => [
                    'description' => 'Record not found.',
                ],
                App::API_FAILED => [
                    'description' => 'Insert operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],

            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => $this->application,
                    'module' => $this->module,
                    'key' => ['update', 'all']
                ],
            ]
        ]);
    }

    public function registerOrmConnections(): void
    {
    }
}
