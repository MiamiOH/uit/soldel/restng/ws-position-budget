<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 12/5/18
 * Time: 2:33 PM
 */

namespace MiamiOH\WSPositionBudget\Repositories;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use MiamiOH\WSPositionBudget\EloquentModels\JobLaborDistributionEloquentModel;
use MiamiOH\WSPositionBudget\Objects\JobLaborDistribution;

class JobLaborDistributionRepositorySQL implements JobLaborDistributionRepository
{
    /**
     * @var MUIDRepository
     */
    private $muidRepository;

    /**
     * PositionBudgetRepositorySQL constructor.
     * @param MUIDRepository $muidRepository
     */
    public function __construct(MUIDRepository $muidRepository)
    {
        $this->muidRepository = $muidRepository;
    }

    /**
     * @param JobLaborDistribution $jobLaborDistribution
     * @return bool
     * @throws \Exception
     */
    public function create(
        JobLaborDistribution $jobLaborDistribution
    ): bool
    {
        $employeeId = $jobLaborDistribution->getMuid();
        $muid = $this->muidRepository->readMUID($employeeId);
        $employeePidm = $muid['pidm'];


        if (empty($employeePidm)) {
            throw new \Exception("Cannot find employee pidm for muid '$employeeId'.");
        }

        $jobLaborDistributionEloquentModel = JobLaborDistributionEloquentModel::create([
            'nbrjlbd_pidm' => $employeePidm,
            'nbrjlbd_posn' => $jobLaborDistribution->getPosition(),
            'nbrjlbd_suff' => $jobLaborDistribution->getSuffix(),
            'nbrjlbd_effective_date' => $jobLaborDistribution->getEffectiveDate(),
            'nbrjlbd_activity_date' => $jobLaborDistribution->getActivityDate(),
            'nbrjlbd_coas_code' => $jobLaborDistribution->getChartOfAccountsCode(),
            'nbrjlbd_acci_code' => $jobLaborDistribution->getAccountIndexCode(),
            'nbrjlbd_fund_code' => $jobLaborDistribution->getFundCode(),
            'nbrjlbd_orgn_code' => $jobLaborDistribution->getOrganizationCode(),
            'nbrjlbd_acct_code' => $jobLaborDistribution->getAccountCode(),
            'nbrjlbd_prog_code' => $jobLaborDistribution->getProgramCode(),
            'nbrjlbd_actv_code' => $jobLaborDistribution->getActivityCode(),
            'nbrjlbd_locn_code' => $jobLaborDistribution->getLocationCode(),
            'nbrjlbd_proj_code' => $jobLaborDistribution->getProjectCode(),
            'nbrjlbd_ctyp_code' => $jobLaborDistribution->getCostTypeCode(),
            'nbrjlbd_acct_code_external' => $jobLaborDistribution->getExternalAccountCode(),
            'nbrjlbd_percent' => $jobLaborDistribution->getPercentage(),
            'nbrjlbd_encd_num' => $jobLaborDistribution->getEncumbranceNumber(),
            'nbrjlbd_encd_seq_num' => $jobLaborDistribution->getEncumbranceSequenceNumber(),
            'nbrjlbd_salary_encumbrance' => $jobLaborDistribution->getSalaryEncumbrance(),
            'nbrjlbd_salary_enc_to_post' => $jobLaborDistribution->getSalaryEncumbranceToPost(),
            'nbrjlbd_fringe_encumbrance' => $jobLaborDistribution->getFringeEncumbrance(),
            'nbrjlbd_fringe_enc_to_post' => $jobLaborDistribution->getFringeEncumbranceToPost(),
            'nbrjlbd_fund_code_fringe' => $jobLaborDistribution->getFringeFund(),
            'nbrjlbd_orgn_code_fringe' => $jobLaborDistribution->getFringeOrganization(),
            'nbrjlbd_acct_code_fringe' => $jobLaborDistribution->getFringeAccount(),
            'nbrjlbd_prog_code_fringe' => $jobLaborDistribution->getFringeProgram(),
            'nbrjlbd_actv_code_fringe' => $jobLaborDistribution->getFringeActivity(),
            'nbrjlbd_locn_code_fringe' => $jobLaborDistribution->getFringeLocation(),
            'nbrjlbd_change_ind' => $jobLaborDistribution->getChangeIndicator(),
            'nbrjlbd_fringe_residual' => $jobLaborDistribution->getFringeEncumbranceResidual(),
            'nbrjlbd_fringe_res_to_post' => $jobLaborDistribution->getFringeEncumbranceResidualToPost(),
            'nbrjlbd_future_salary_enc' => $jobLaborDistribution->getFutureSalaryEncumbrance(),
            'nbrjlbd_future_fringe_enc' => $jobLaborDistribution->getFutureFringeEncumbrance(),
            'nbrjlbd_future_fringe_residual' => $jobLaborDistribution->getFutureFringeEncumbranceResidual(),
            'nbrjlbd_future_sal_enc_to_post' => $jobLaborDistribution->getFutureSalaryEncumbranceToPost(),
            'nbrjlbd_future_frg_enc_to_post' => $jobLaborDistribution->getFutureFringeEncumbranceToPost(),
            'nbrjlbd_future_frg_res_to_post' => $jobLaborDistribution->getFutureFringeEncumbranceResidualToPost(),
            'nbrjlbd_enc_override_end_date' => $jobLaborDistribution->getEncumbranceOverrideEndDate(),
            'nbrjlbd_enc_last_recast_date' => $jobLaborDistribution->getEncumbranceLatestRecastDate(),
            'nbrjlbd_enc_last_calc_date' => $jobLaborDistribution->getEncumbranceLastCalculatedDate(),
            'nbrjlbd_encd_num_fringe' => $jobLaborDistribution->getFringeEncumbranceNumber(),
            'nbrjlbd_encd_seq_num_fringe' => $jobLaborDistribution->getFringeEncumbranceSequenceNumber(),
            'nbrjlbd_user_id' => $jobLaborDistribution->getUserId(),
            'nbrjlbd_data_origin' => $jobLaborDistribution->getDataOrigin(),
        ]);

        return true;
    }

    /**
     * @param JobLaborDistribution $jobLaborDistribution
     * @return bool
     * @throws \Exception
     */
    public function update(
        JobLaborDistribution $jobLaborDistribution
    ): bool
    {
        try {
            $jobLaborDistributionModel = JobLaborDistributionEloquentModel::where('nbrjlbd_surrogate_id', $jobLaborDistribution->getSurrogateId())->firstOrFail();
        } catch (ModelNotFoundException $e) {
            throw new ModelNotFoundException('Surrogate not found.');
        }

        $modifiedAttributes = $jobLaborDistribution->getModifiedAttributes();

        if (isset($modifiedAttributes['pidm'])) {
            $employeeId = $jobLaborDistribution->getMuid();
            $muid = $this->muidRepository->readMUID($employeeId);
            $employeePidm = $muid['pidm'];
            $jobLaborDistributionModel->nbrjlbd_pidm = $employeePidm;
        };

        if (isset($modifiedAttributes['position'])) {
            $jobLaborDistributionModel->nbrjlbd_posn = $jobLaborDistribution->getPosition();
        };

        if (isset($modifiedAttributes['suffix'])) {
            $jobLaborDistributionModel->nbrjlbd_suff = $jobLaborDistribution->getSuffix();
        };

        if (isset($modifiedAttributes['effectiveDate'])) {
            $jobLaborDistributionModel->nbrjlbd_effective_date = $jobLaborDistribution->getEffectiveDate();
        };

        if (isset($modifiedAttributes['chartOfAccountsCode'])) {
            $jobLaborDistributionModel->nbrjlbd_coas_code = $jobLaborDistribution->getChartOfAccountsCode();
        };

        if (isset($modifiedAttributes['accountIndexCode'])) {
            $jobLaborDistributionModel->nbrjlbd_acci_code = $jobLaborDistribution->getAccountIndexCode();
        };

        if (isset($modifiedAttributes['fundCode'])) {
            $jobLaborDistributionModel->nbrjlbd_fund_code = $jobLaborDistribution->getFundCode();
        };

        if (isset($modifiedAttributes['organizationCode'])) {
            $jobLaborDistributionModel->nbrjlbd_orgn_code = $jobLaborDistribution->getOrganizationCode();
        };

        if (isset($modifiedAttributes['accountCode'])) {
            $jobLaborDistributionModel->nbrjlbd_acct_code = $jobLaborDistribution->getAccountCode();
        };

        if (isset($modifiedAttributes['programCode'])) {
            $jobLaborDistributionModel->nbrjlbd_prog_code = $jobLaborDistribution->getProgramCode();
        };

        if (isset($modifiedAttributes['activityCode'])) {
            $jobLaborDistributionModel->nbrjlbd_actv_code = $jobLaborDistribution->getActivityCode();
        };

        if (isset($modifiedAttributes['locationCode'])) {
            $jobLaborDistributionModel->nbrjlbd_locn_code = $jobLaborDistribution->getLocationCode();
        };

        if (isset($modifiedAttributes['projectCode'])) {
            $jobLaborDistributionModel->nbrjlbd_proj_code = $jobLaborDistribution->getProjectCode();
        };

        if (isset($modifiedAttributes['costTypeCode'])) {
            $jobLaborDistributionModel->nbrjlbd_ctyp_code = $jobLaborDistribution->getCostTypeCode();
        };

        if (isset($modifiedAttributes['externalAccountCode'])) {
            $jobLaborDistributionModel->nbrjlbd_acct_code_external = $jobLaborDistribution->getExternalAccountCode();
        };

        if (isset($modifiedAttributes['percentage'])) {
            $jobLaborDistributionModel->nbrjlbd_percent = $jobLaborDistribution->getPercentage();
        };

        if (isset($modifiedAttributes['encumbranceNumber'])) {
            $jobLaborDistributionModel->nbrjlbd_encd_num = $jobLaborDistribution->getEncumbranceNumber();
        };

        if (isset($modifiedAttributes['encumbranceSequenceNumber'])) {
            $jobLaborDistributionModel->nbrjlbd_encd_seq_num = $jobLaborDistribution->getEncumbranceSequenceNumber();
        };

        if (isset($modifiedAttributes['salaryEncumbrance'])) {
            $jobLaborDistributionModel->nbrjlbd_salary_encumbrance = $jobLaborDistribution->getSalaryEncumbrance();
        };

        if (isset($modifiedAttributes['salaryEncumbranceToPost'])) {
            $jobLaborDistributionModel->nbrjlbd_salary_enc_to_post = $jobLaborDistribution->getSalaryEncumbranceToPost();
        };

        if (isset($modifiedAttributes['fringeEncumbrance'])) {
            $jobLaborDistributionModel->nbrjlbd_fringe_encumbrance = $jobLaborDistribution->getFringeEncumbrance();
        };

        if (isset($modifiedAttributes['fringeEncumbranceToPost'])) {
            $jobLaborDistributionModel->nbrjlbd_fringe_enc_to_post = $jobLaborDistribution->getFringeEncumbranceToPost();
        };

        if (isset($modifiedAttributes['fringeFund'])) {
            $jobLaborDistributionModel->nbrjlbd_fund_code_fringe = $jobLaborDistribution->getFringeFund();
        };

        if (isset($modifiedAttributes['fringeOrganization'])) {
            $jobLaborDistributionModel->nbrjlbd_orgn_code_fringe = $jobLaborDistribution->getFringeOrganization();
        };

        if (isset($modifiedAttributes['fringeAccount'])) {
            $jobLaborDistributionModel->nbrjlbd_acct_code_fringe = $jobLaborDistribution->getFringeAccount();
        };

        if (isset($modifiedAttributes['fringeProgram'])) {
            $jobLaborDistributionModel->nbrjlbd_prog_code_fringe = $jobLaborDistribution->getFringeProgram();
        };

        if (isset($modifiedAttributes['fringeActivity'])) {
            $jobLaborDistributionModel->nbrjlbd_actv_code_fringe = $jobLaborDistribution->getFringeActivity();
        };

        if (isset($modifiedAttributes['fringeLocation'])) {
            $jobLaborDistributionModel->nbrjlbd_locn_code_fringe = $jobLaborDistribution->getFringeLocation();
        };

        if (isset($modifiedAttributes['changeIndicator'])) {
            $jobLaborDistributionModel->nbrjlbd_change_ind = $jobLaborDistribution->getChangeIndicator();
        };

        if (isset($modifiedAttributes['fringeEncumbranceResidual'])) {
            $jobLaborDistributionModel->nbrjlbd_fringe_residual = $jobLaborDistribution->getFringeEncumbranceResidual();
        };

        if (isset($modifiedAttributes['fringeEncumbranceResidualToPost'])) {
            $jobLaborDistributionModel->nbrjlbd_fringe_res_to_post = $jobLaborDistribution->getFringeEncumbranceResidualToPost();
        };

        if (isset($modifiedAttributes['futureSalaryEncumbrance'])) {
            $jobLaborDistributionModel->nbrjlbd_future_salary_enc = $jobLaborDistribution->getFutureSalaryEncumbrance();
        };

        if (isset($modifiedAttributes['futureFringeEncumbrance'])) {
            $jobLaborDistributionModel->nbrjlbd_future_fringe_enc = $jobLaborDistribution->getFutureFringeEncumbrance();
        };

        if (isset($modifiedAttributes['futueFringeEncumbranceResidual'])) {
            $jobLaborDistributionModel->nbrjlbd_future_fringe_residual = $jobLaborDistribution->getFutureFringeEncumbranceResidual();
        };

        if (isset($modifiedAttributes['futureSalaryEncumbranceToPost'])) {
            $jobLaborDistributionModel->nbrjlbd_future_sal_enc_to_post = $jobLaborDistribution->getFutureSalaryEncumbranceToPost();
        };

        if (isset($modifiedAttributes['futureFringeEncumbranceToPost'])) {
            $jobLaborDistributionModel->nbrjlbd_future_frg_enc_to_post = $jobLaborDistribution->getFutureFringeEncumbranceToPost();
        };

        if (isset($modifiedAttributes['futueFringeEncumbranceResidualToPost'])) {
            $jobLaborDistributionModel->nbrjlbd_future_frg_res_to_post = $jobLaborDistribution->getFutureFringeEncumbranceResidualToPost();
        };

        if (isset($modifiedAttributes['encumbranceOverrideEndDate'])) {
            $jobLaborDistributionModel->nbrjlbd_enc_override_end_date = $jobLaborDistribution->getEncumbranceOverrideEndDate();
        };

        if (isset($modifiedAttributes['encumbranceLatestRecastDate'])) {
            $jobLaborDistributionModel->nbrjlbd_enc_last_recast_date = $jobLaborDistribution->getEncumbranceLatestRecastDate();
        };

        if (isset($modifiedAttributes['encumbranceLastCalculatedDate'])) {
            $jobLaborDistributionModel->nbrjlbd_enc_last_calc_date = $jobLaborDistribution->getEncumbranceLastCalculatedDate();
        };

        if (isset($modifiedAttributes['fringeEncumbranceNumber'])) {
            $jobLaborDistributionModel->nbrjlbd_encd_num_fringe = $jobLaborDistribution->getEncumbranceNumber();
        };

        if (isset($modifiedAttributes['fringeEncumbranceSequenceNumber'])) {
            $jobLaborDistributionModel->nbrjlbd_encd_seq_num_fringe = $jobLaborDistribution->getEncumbranceSequenceNumber();
        };

        if (isset($modifiedAttributes['surrogateId'])) {
            $jobLaborDistributionModel->nbrjlbd_surrogate_id = $jobLaborDistribution->getSurrogateId();
        };

        $jobLaborDistributionModel->nbrjlbd_activity_date = $jobLaborDistribution->getActivityDate();
        $jobLaborDistributionModel->nbrjlbd_user_id = strtoupper($jobLaborDistribution->getUserId());
        $jobLaborDistributionModel->nbrjlbd_data_origin = $jobLaborDistribution->getDataOrigin();

        $jobLaborDistributionModel->save();

        return true;
    }

    public function read(string $muid, string $suffix, string $position)
    {
        $muid = $this->muidRepository->readMUID($muid);
        $employeePidm = $muid['pidm'];

        $data = JobLaborDistributionEloquentModel::where(
            'nbrjlbd_pidm', '=', $employeePidm)
            ->where('nbrjlbd_suff', $suffix)
            ->where('nbrjlbd_posn', $position)
            ->where('nbrjlbd_change_ind', 'A')
            ->whereRaw("TRUNC(nbrjlbd_effective_date) = (select max(nbrjlbd_effective_date)
                                                     from nbrjlbd
                                                     where nbrjlbd_pidm = $employeePidm
                                                     and nbrjlbd_posn = $position
                                                     and nbrjlbd_suff = $suffix
                                                     and nbrjlbd_change_ind = 'A'
                                                     and nbrjlbd_effective_date <= sysdate)")
            ->get();

        $jobDistribution = [];

        foreach ($data as $datum) {
            $jobDistributionData['pidm'] = $datum['nbrjlbd_pidm'];
            $jobDistributionData['position'] = $datum['nbrjlbd_posn'];
            $jobDistributionData['suffix'] = $datum['nbrjlbd_suff'];
            $jobDistributionData['effectiveDate'] = $datum['nbrjlbd_effective_date'];
            $jobDistributionData['chartOfAccountsCode'] = $datum['nbrjlbd_coas_code'];
            $jobDistributionData['accountIndexCode'] = $datum['nbrjlbd_acci_code'];
            $jobDistributionData['fundCode'] = $datum['nbrjlbd_fund_code'];
            $jobDistributionData['organizationCode'] = $datum['nbrjlbd_orgn_code'];
            $jobDistributionData['accountCode'] = $datum['nbrjlbd_acct_code'];
            $jobDistributionData['programCode'] = $datum['nbrjlbd_prog_code'];
            $jobDistributionData['activityCode'] = $datum['nbrjlbd_actv_code'];
            $jobDistributionData['locationCode'] = $datum['nbrjlbd_locn_code'];
            $jobDistributionData['projectCode'] = $datum['nbrjlbd_proj_code'];
            $jobDistributionData['costTypeCode'] = $datum['nbrjlbd_ctyp_code'];
            $jobDistributionData['externalAccountCode'] = $datum['nbrjlbd_acct_code_external'];
            $jobDistributionData['percentage'] = $datum['nbrjlbd_percent'];
            $jobDistributionData['encumbranceNumber'] = $datum['nbrjlbd_encd_num'];
            $jobDistributionData['encumbranceSequenceNumber'] = $datum['nbrjlbd_encd_seq_num'];
            $jobDistributionData['salaryEncumbrance'] = $datum['nbrjlbd_salary_encumbrance'];
            $jobDistributionData['salaryEncumbranceToPost'] = $datum['nbrjlbd_salary_enc_to_post'];
            $jobDistributionData['fringeEncumbrance'] = $datum['nbrjlbd_fringe_encumbrance'];
            $jobDistributionData['fringeEncumbranceToPost'] = $datum['nbrjlbd_fringe_enc_to_post'];
            $jobDistributionData['fringeFund'] = $datum['nbrjlbd_fund_code_fringe'];
            $jobDistributionData['fringOrganization'] = $datum['nbrjlbd_orgn_code_fringe'];
            $jobDistributionData['fringeAccount'] = $datum['nbrjlbd_acct_code_fringe'];
            $jobDistributionData['fringeProgram'] = $datum['nbrjlbd_prog_code_fringe'];
            $jobDistributionData['fringeActivity'] = $datum['nnbrjlbd_actv_code_fringe'];
            $jobDistributionData['fringeLocation'] = $datum['nbrjlbd_locn_code_fringe'];
            $jobDistributionData['fringeEncumbranceResidual'] = $datum['nbrjlbd_fringe_residual'];
            $jobDistributionData['fringeEncumbranceResidualToPost'] = $datum['nbrjlbd_fringe_res_to_post'];
            $jobDistributionData['futureSalaryEncumbrance'] = $datum['nbrjlbd_future_salary_enc'];
            $jobDistributionData['futureFringeEncumbrance'] = $datum['nbrjlbd_future_fringe_enc'];
            $jobDistributionData['futureFringeEncumbranceResidual'] = $datum['nbrjlbd_future_fringe_residual'];
            $jobDistributionData['futureSalaryEncumbranceToPost'] = $datum['nbrjlbd_future_sal_enc_to_post'];
            $jobDistributionData['futureFringeEncumbranceToPost'] = $datum['nbrjlbd_future_frg_enc_to_post'];
            $jobDistributionData['futureFringeEncumbranceResidualToPost'] = $datum['nbrjlbd_future_frg_res_to_post'];
            $jobDistributionData['encumbranceOverrideEndDate'] = $datum['nbrjlbd_enc_override_end_date'];
            $jobDistributionData['encumbranceLatestRecastDate'] = $datum['nbrjlbd_enc_last_recast_date'];
            $jobDistributionData['encumbranceLastCalculatedDate'] = $datum['nbrjlbd_enc_last_calc_date'];
            $jobDistributionData['fringeEncumbranceNumber'] = $datum['nbrjlbd_encd_num_fringe'];
            $jobDistributionData['fringeEncumbranceSequenceNumber'] = $datum['nbrjlbd_encd_seq_num_fringe'];
            $jobDistributionData['activityDate'] = $datum['nbrjlbd_activity_date'];
            $jobDistributionData['changeIndicator'] = $datum['nbrjlbd_change_ind'];
            $jobDistributionData['userId'] = $datum['nbrjlbd_user_id'];
            $jobDistributionData['dataOrigin'] = $datum['nbrjlbd_data_origin'];
            $jobDistributionData['surrogateId'] = $datum['nbrjlbd_surrogate_id'];
            $jobDistribution[] = $jobDistributionData;
        }

        return $jobDistribution;
    }
}
