<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 12/4/18
 * Time: 4:50 PM
 */

namespace MiamiOH\WSPositionBudget\Repositories;

use MiamiOH\WSPositionBudget\Objects\PositionBudget;

interface PositionBudgetRepository
{
    public function get(string $positionNumber, int $year);
    public function create(PositionBudget $positionBudget): bool;
    public function update(PositionBudget $positionBudget);
}
