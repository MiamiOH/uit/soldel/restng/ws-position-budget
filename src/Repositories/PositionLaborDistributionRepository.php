<?php

namespace MiamiOH\WSPositionBudget\Repositories;

use MiamiOH\WSPositionBudget\Objects\PositionLaborDistribution;

interface PositionLaborDistributionRepository
{
    public function create(PositionLaborDistribution $positionLaborDistribution): bool;

    public function update(PositionLaborDistribution $positionLaborDistribution): bool;

    public function get(string $position, string $fiscalYear);

}
