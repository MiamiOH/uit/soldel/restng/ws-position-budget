<?php

namespace MiamiOH\WSPositionBudget\Repositories;


use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use MiamiOH\WSPositionBudget\EloquentModels\AccountIndexEloquentModel;
use MiamiOH\WSPositionBudget\EloquentModels\PositionLaborDistributionEloquentModel;
use MiamiOH\WSPositionBudget\Objects\PositionLaborDistribution;

class PositionLaborDistributionRepositorySQL implements PositionLaborDistributionRepository
{
    /**
     * @param PositionLaborDistribution $jobLaborDistribution
     * @return array
     * @throws \Exception
     */
    public function create(
        PositionLaborDistribution $positionLaborDistribution
    ): bool
    {
        $positionLaborDistributionEloquentModel = PositionLaborDistributionEloquentModel::create([
            'nbrplbd_acci_code' => $positionLaborDistribution->getAccountIndexCode(),
            'nbrplbd_acct_code' => $positionLaborDistribution->getAccountCode(),
            'nbrplbd_acct_code_external' => $positionLaborDistribution->getExternalAccountCode(),
            'nbrplbd_activity_date' => $positionLaborDistribution->getActivityDate(),
            'nbrplbd_actv_code' => $positionLaborDistribution->getActivityCode(),
            'nbrplbd_budget' => $positionLaborDistribution->getBudget(),
            'nbrplbd_budget_to_post' => $positionLaborDistribution->getBudgetToPost(),
            'nbrplbd_change_ind' => $positionLaborDistribution->getChangeIndicator(),
            'nbrplbd_coas_code' => $positionLaborDistribution->getChartOfAccountsCode(),
            'nbrplbd_ctyp_code' => $positionLaborDistribution->getCostTypeCode(),
            'nbrplbd_data_origin' => $positionLaborDistribution->getDataOrigin(),
            'nbrplbd_fisc_code' => $positionLaborDistribution->getFiscalYearCode(),
            'nbrplbd_fund_code' => $positionLaborDistribution->getFundCode(),
            'nbrplbd_locn_code' => $positionLaborDistribution->getLocationCode(),
            'nbrplbd_obph_code' => $positionLaborDistribution->getBudgetPhaseCode(),
            'nbrplbd_obud_code' => $positionLaborDistribution->getBudgetIdCode(),
            'nbrplbd_orgn_code' => $positionLaborDistribution->getOrganizationCode(),
            'nbrplbd_percent' => $positionLaborDistribution->getPercentage(),
            'nbrplbd_posn' => $positionLaborDistribution->getPosition(),
            'nbrplbd_prog_code' => $positionLaborDistribution->getProgramCode(),
            'nbrplbd_proj_code' => $positionLaborDistribution->getProjectCode(),
        ]);

        return true;
    }

    /**
     * @param PositionLaborDistribution $positionLaborDistribution
     * @return bool
     * @throws \Exception
     */
    public function update(
        PositionLaborDistribution $positionLaborDistribution
    ): bool
    {
        try {
            $laborDistributionModel = PositionLaborDistributionEloquentModel::where('nbrplbd_surrogate_id', $positionLaborDistribution->getSurrogateId())->firstOrFail();
        } catch (ModelNotFoundException $e) {
            throw new ModelNotFoundException('Surrogate not found.');
        }

        $modifiedAttributes = $positionLaborDistribution->getModifiedAttributes();

        if (isset($modifiedAttributes['position'])) {
            $laborDistributionModel->nbrplbd_posn = $positionLaborDistribution->getPosition();
        };

        if (isset($modifiedAttributes['fiscalYearCode'])) {
            $laborDistributionModel->nbrplbd_fisc_code = $positionLaborDistribution->getFiscalYearCode();
        };

        if (isset($modifiedAttributes['chartOfAccountsCode'])) {
            $laborDistributionModel->nbrplbd_coas_code = $positionLaborDistribution->getChartOfAccountsCode();
        };

        if (isset($modifiedAttributes['accountIndexCode'])) {
            $laborDistributionModel->nbrplbd_acci_code = $positionLaborDistribution->getAccountIndexCode();
        };

        if (isset($modifiedAttributes['fundCode'])) {
            $laborDistributionModel->nbrplbd_fund_code = $positionLaborDistribution->getFundCode();
        };

        if (isset($modifiedAttributes['organizationCode'])) {
            $laborDistributionModel->nbrplbd_orgn_code = $positionLaborDistribution->getOrganizationCode();
        };

        if (isset($modifiedAttributes['accountCode'])) {
            $laborDistributionModel->nbrplbd_acct_code = $positionLaborDistribution->getAccountCode();
        };

        if (isset($modifiedAttributes['programCode'])) {
            $laborDistributionModel->nbrplbd_prog_code = $positionLaborDistribution->getProgramCode();
        };

        if (isset($modifiedAttributes['activityCode'])) {
            $laborDistributionModel->nbrplbd_actv_code = $positionLaborDistribution->getActivityCode();
        };

        if (isset($modifiedAttributes['locationCode'])) {
            $laborDistributionModel->nbrplbd_locn_code = $positionLaborDistribution->getLocationCode();
        };

        if (isset($modifiedAttributes['externalAccountCode'])) {
            $laborDistributionModel->nbrplbd_acct_code_external = $positionLaborDistribution->getExternalAccountCode();
        };

        if (isset($modifiedAttributes['percentage'])) {
            $laborDistributionModel->nbrplbd_percent = $positionLaborDistribution->getPercentage();
        };

        if (isset($modifiedAttributes['budgetPhaseCode'])) {
            $laborDistributionModel->nbrplbd_obph_code = $positionLaborDistribution->getBudgetPhaseCode();
        };

        if (isset($modifiedAttributes['budgetIdCode'])) {
            $laborDistributionModel->nbrplbd_obud_code = $positionLaborDistribution->getBudgetIdCode();
        };

        if (isset($modifiedAttributes['changeIndicator'])) {
            $laborDistributionModel->nbrplbd_change_ind = $positionLaborDistribution->getChangeIndicator();
        };

        if (isset($modifiedAttributes['projectCode'])) {
            $laborDistributionModel->nbrplbd_proj_code = $positionLaborDistribution->getProjectCode();
        };

        if (isset($modifiedAttributes['budget'])) {
            $laborDistributionModel->nbrplbd_budget = $positionLaborDistribution->getBudget();
        };

        if (isset($modifiedAttributes['budgetToPost'])) {
            $laborDistributionModel->nbrplbd_budget_to_post = $positionLaborDistribution->getBudgetToPost();
        };

        if (isset($modifiedAttributes['costTypeCode'])) {
            $laborDistributionModel->nbrplbd_ctyp_code = $positionLaborDistribution->getCostTypeCode();
        };

        if (isset($modifiedAttributes['surrogateId'])) {
            $laborDistributionModel->nbrplbd_surrogate_id = $positionLaborDistribution->getSurrogateId();
        };

        $laborDistributionModel->nbrplbd_activity_date = $positionLaborDistribution->getActivityDate();
        $laborDistributionModel->nbrplbd_user_id = strtoupper($positionLaborDistribution->getUserId());
        $laborDistributionModel->nbrplbd_data_origin = $positionLaborDistribution->getDataOrigin();

        $laborDistributionModel->save();

        return true;
    }

    public function get(string $position, string $fiscalYear)
    {
        $data = PositionLaborDistributionEloquentModel
            ::where('nbrplbd_posn', '=', $position)
            ->where('nbrplbd_fisc_code', '=', $fiscalYear)
            ->whereNull('nbrplbd_change_ind')
            ->get();

        $positionDistribution = [];

        foreach ($data as $datum) {
            $positionDistributionData = [];
            $positionDistributionData['position'] = $datum['nbrplbd_posn'];
            $positionDistributionData['fiscalYearCode'] = $datum['nbrplbd_fisc_code'];
            $positionDistributionData['chartOfAccountsCode'] = $datum['nbrplbd_coas_code'];
            $positionDistributionData['accountIndexCode'] = $datum['nbrplbd_acci_code'];
            $accountIndexCodeDescription = AccountIndexEloquentModel::select('ftvacci_title')->where('ftvacci_acci_code', '=', $datum['nbrplbd_acci_code'])->first();
            $positionDistributionData['accountIndexCodeDescription'] = $accountIndexCodeDescription->ftvacci_title ?? '';
            $positionDistributionData['fundCode'] = $datum['nbrplbd_fund_code'];
            $positionDistributionData['organizationCode'] = $datum['nbrplbd_orgn_code'];
            $positionDistributionData['accountCode'] = $datum['nbrplbd_acct_code'];
            $positionDistributionData['programCode'] = $datum['nbrplbd_prog_code'];
            $positionDistributionData['activityCode'] = $datum['nbrplbd_actv_code'];
            $positionDistributionData['locationCode'] = $datum['nbrplbd_locn_code'];
            $positionDistributionData['externalAccountCode'] = $datum['nbrplbd_acct_code_external'];
            $positionDistributionData['percentage'] = $datum['nbrplbd_percent'];
            $positionDistributionData['activityDate'] = $datum['nbrplbd_activity_date'];
            $positionDistributionData['budgetIdCode'] = $datum['nbrplbd_obud_code'];
            $positionDistributionData['budgetPhaseCode'] = $datum['nbrplbd_obph_code'];
            $positionDistributionData['changeIndicator'] = $datum['nbrplbd_change_ind'];
            $positionDistributionData['projectCode'] = $datum['nbrplbd_proj_code'];
            $positionDistributionData['budget'] = $datum['nbrplbd_budget'];
            $positionDistributionData['budgetToPost'] = $datum['nbrplbd_budget_to_post'];
            $positionDistributionData['costTypeCode'] = $datum['nbrplbd_ctyp_code'];
            $positionDistributionData['surrogateId'] = $datum['nbrplbd_surrogate_id'];
            $positionDistribution[] = $positionDistributionData;
        }

        return $positionDistribution;
    }
}
