<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 12/5/18
 * Time: 2:32 PM
 */

namespace MiamiOH\WSPositionBudget\Repositories;

use MiamiOH\WSPositionBudget\Objects\JobLaborDistribution;

interface JobLaborDistributionRepository
{
    public function create(JobLaborDistribution $jobLaborDistribution): bool;

    public function update(JobLaborDistribution $positionLaborDistribution): bool;

    public function read(string $muid, string $suffix, string $position);
}
