<?php

namespace MiamiOH\WSPositionBudget\Repositories;

use MiamiOH\WSPositionBudget\EloquentModels\PositionBudgetEloquentModel;
use MiamiOH\WSPositionBudget\Objects\PositionBudget;
use Illuminate\Database\Capsule\Manager as DB;

/**
 * Class PositionBudgetRepositorySQL
 */
class PositionBudgetRepositorySQL implements PositionBudgetRepository
{
    public function get(string $positionNumber, int $year)
    {
        $positionBudget = new PositionBudgetEloquentModel();

        $budgetDetail = $positionBudget::where('nbrptot_posn', '=', $positionNumber)
            ->where('nbrptot_fisc_code', '=', $year)
            ->orderBy('nbrptot_effective_date', 'desc')->first();

        $savingAccount = $positionBudget::select('nbrptot_posn', 'nbrptot_budget')
            ->join('nbbposn', function($join) {
                $join->on('nbrptot_posn', 'nbbposn_auth_number');
            })
            ->where('nbbposn_posn', $positionNumber)
            ->where('nbrptot_fisc_code', '=', $year)
            ->first();

        if ($budgetDetail === null) {
            return null;
        }

        $budget= [];

        $budget['position'] = $budgetDetail['nbrptot_posn'];
        $budget['fiscalYearCode'] = $budgetDetail['nbrptot_fisc_code'];
        $budget['effectiveDate'] = $budgetDetail['nbrptot_effective_date'];
        $budget['savingAccountPosition'] = $savingAccount->nbrptot_posn ?? '';
        $budget['savingAccountBudget'] = $savingAccount->nbrptot_budget ?? '';
        $budget['status'] = $budgetDetail['nbrptot_status'];
        $budget['fullTimeEquivalency'] = $budgetDetail['nbrptot_fte'];
        $budget['chartOfAccountsCode'] = $budgetDetail['nbrptot_coas_code'];
        $budget['organizationCode'] = $budgetDetail['nbrptot_orgn_code'];
        $budget['budget'] = $budgetDetail['nbrptot_budget'];
        $budget['encumbranceAmount'] = $budgetDetail['nbrptot_encumb'];
        $budget['expenditureAmount'] = $budgetDetail['nbrptot_expend'];
        $budget['budgetIdCode'] = $budgetDetail['nbrptot_obud_code'];
        $budget['budgetPhaseCode'] = $budgetDetail['nbrptot_obph_code'];
        $budget['salaryGroupCode'] = $budgetDetail['nbrptot_sgrp_code'];
        $budget['accountIndexCode'] = $budgetDetail['nbrptot_acci_code_frng'];
        $budget['positionBudgetFringe'] = $budgetDetail['nbrptot_budget_frng'];
        $budget['fringeEncumbrance'] = $budgetDetail['nbrptot_encumb_frng'];
        $budget['fringeExpenditure'] = $budgetDetail['nbrptot_expend_frng'];
        $budget['fringeFundCode'] = $budgetDetail['nbrptot_fund_code_frng'];
        $budget['fringeOrganizationCode'] = $budgetDetail['nbrptot_orgn_code_frng'];
        $budget['fringeAccountCode'] = $budgetDetail['nbrptot_acct_code_frng'];
        $budget['fringeProgramCode'] = $budgetDetail['nbrptot_prog_code_frng'];
        $budget['fringeActivityCode'] = $budgetDetail['nbrptot_actv_code_frng'];
        $budget['fringeLocationCode'] = $budgetDetail['nbrptot_locn_code_frng'];
        $budget['recurringBudgetAmount'] = $budgetDetail['nbrptot_recurring_budget'];
        $budget['positionBudgetBasis'] = $budgetDetail['nbrptot_budg_basis'];
        $budget['positionAnnualBasis'] = $budgetDetail['nbrptot_ann_basis'];
        $budget['baseUnits'] = $budgetDetail['nbrptot_base_units'];
        $budget['appointmentPercent'] = $budgetDetail['nbrptot_appt_pct'];
        $budget['createNbrjfteIndicator'] = $budgetDetail['nbrptot_create_jfte_ind'];
        $budget['comment'] = $budgetDetail['nbrptot_comment'];
        $budget['dataOrigin'] = $budgetDetail['nbrptot_data_origin'];
        $budget['userId'] = $budgetDetail['nbrptot_user_id'];

        return $budget;
    }

    /**
     * @param PositionBudget $positionBudget
     * @return bool
     * @throws \Exception
     */
    public function create(
        PositionBudget $positionBudget
    ): bool {
        $positionBudgetEloquentModel = PositionBudgetEloquentModel::create([
            'nbrptot_posn' => $positionBudget->getPosition(),
            'nbrptot_fisc_code' => $positionBudget->getFiscalYearCode(),
            'nbrptot_effective_date' => $positionBudget->getEffectiveDate(),
            'nbrptot_activity_date' => $positionBudget->getActivityDate(),
            'nbrptot_status' => $positionBudget->getStatus(),
            'nbrptot_fte' => $positionBudget->getFullTimeEquivalency(),
            'nbrptot_coas_code' => $positionBudget->getChartOfAccountsCode(),
            'nbrptot_orgn_code' => $positionBudget->getOrganizationCode(),
            'nbrptot_budget' => $positionBudget->getBudget(),
            'nbrptot_encumb' => $positionBudget->getEncumbranceAmount(),
            'nbrptot_expend' => $positionBudget->getExpenditureAmount(),
            'nbrptot_obud_code' => $positionBudget->getBudgetIdCode(),
            'nbrptot_obph_code' => $positionBudget->getBudgetPhaseCode(),
            'nbrptot_sgrp_code' => $positionBudget->getSalaryGroupCode(),
            'nbrptot_budget_frng' => $positionBudget->getPositionBudgetFringe(),
            'nbrptot_encumb_frng' => $positionBudget->getEncumbranceAmount(),
            'nbrptot_expend_frng' => $positionBudget->getExpenditureAmount(),
            'nbrptot_acci_code_frng' => $positionBudget->getAccountIndexCode(),
            'nbrptot_fund_code_frng' => $positionBudget->getFringeFundCode(),
            'nbrptot_orgn_code_frng' => $positionBudget->getFringeOrganizationCode(),
            'nbrptot_acct_code_frng' => $positionBudget->getFringeAccountCode(),
            'nbrptot_prog_code_frng' => $positionBudget->getFringeProgramCode(),
            'nbrptot_actv_code_frng' => $positionBudget->getFringeActivityCode(),
            'nbrptot_locn_code_frng' => $positionBudget->getFringeLocationCode(),
            'nbrptot_recurring_budget' => $positionBudget->getRecurringBudgetAmount(),
            'nbrptot_budg_basis' => $positionBudget->getPositionBudgetBasis(),
            'nbrptot_ann_basis' => $positionBudget->getPositionAnnualBasis(),
            'nbrptot_base_units' => $positionBudget->getBaseUnits(),
            'nbrptot_appt_pct' => $positionBudget->getAppointmentPercent(),
            'nbrptot_create_jfte_ind' => $positionBudget->getCreateNbrjfteIndicator(),
            'nbrptot_user_id' => $positionBudget->getUserId(),
            'nbrptot_data_origin' => $positionBudget->getDataOrigin(),
            'nbrptot_comment' => $positionBudget->getComment(),
        ]);

        return true;
    }

    public function update(PositionBudget $positionBudget)
    {
        $model = PositionBudgetEloquentModel::where('nbrptot_posn', $positionBudget->getPosition())
            ->where('nbrptot_fisc_code', '=', $positionBudget->getFiscalYearCode())
            ->first();

        if ($model === null) {
            return null;
        }

        $modifiedAttributes = $positionBudget->getModifiedAttributes();

        if (isset($modifiedAttributes['effectiveDate'])) {
            $model->nbrptot_effective_date = $positionBudget->getEffectiveDate();
        }

        if (isset($modifiedAttributes['status'])) {
            $model->nbrptot_status = $positionBudget->getStatus();
        }

        if (isset($modifiedAttributes['fullTimeEquivalency'])) {
            $model->nbrptot_fte = $positionBudget->getFullTimeEquivalency();
        }

        if (isset($modifiedAttributes['chartOfAccountsCode'])) {
            $model->nbrptot_coas_code = $positionBudget->getChartOfAccountsCode();
        }

        if (isset($modifiedAttributes['organizationCode'])) {
            $model->nbrptot_orgn_code = $positionBudget->getOrganizationCode();
        }

        if (isset($modifiedAttributes['budget'])) {
            $model->nbrptot_budget = $positionBudget->getBudget();
        }

        if (isset($modifiedAttributes['encumbranceAmount'])) {
            $model->nbrptot_encumb = $positionBudget->getEncumbranceAmount();
        }

        if (isset($modifiedAttributes['expenditureAmount'])) {
            $model->nbrptot_expend = $positionBudget->getExpenditureAmount();
        }

        if (isset($modifiedAttributes['budgetIdCode'])) {
            $model->nbrptot_obud_code = $positionBudget->getBudgetIdCode();
        }

        if (isset($modifiedAttributes['budgetPhaseCode'])) {
            $model->nbrptot_obph_code = $positionBudget->getBudgetPhaseCode();
        }

        if (isset($modifiedAttributes['salaryGroupCode'])) {
            $model->nbrptot_sgrp_code = $positionBudget->getSalaryGroupCode();
        }

        if (isset($modifiedAttributes['accountIndexCode'])) {
            $model->nbrptot_acci_code_frng = $positionBudget->getAccountIndexCode();
        }

        if (isset($modifiedAttributes['positionBudgetFringe'])) {
            $model->nbrptot_budget_frng = $positionBudget->getPositionBudgetFringe();
        }

        if (isset($modifiedAttributes['fringeEncumbrance'])) {
            $model->nbrptot_encumb_frng = $positionBudget->getFringeEncumbrance();
        }

        if (isset($modifiedAttributes['fringeExpenditure'])) {
            $model->nbrptot_expend_frng = $positionBudget->getFringeExpenditure();
        }

        if (isset($modifiedAttributes['fringeFundCode'])) {
            $model->nbrptot_fund_code_frng = $positionBudget->getFringeFundCode();
        }

        if (isset($modifiedAttributes['fringeOrganizationCode'])) {
            $model->nbrptot_orgn_code_frng = $positionBudget->getFringeOrganizationCode();
        }

        if (isset($modifiedAttributes['fringeAccountCode'])) {
            $model->nbrptot_acct_code_frng = $positionBudget->getFringeAccountCode();
        }

        if (isset($modifiedAttributes['fringeProgramCode'])) {
            $model->nbrptot_prog_code_frng = $positionBudget->getFringeProgramCode();
        }

        if (isset($modifiedAttributes['fringeActivityCode'])) {
            $model->nbrptot_actv_code_frng = $positionBudget->getFringeActivityCode();
        }

        if (isset($modifiedAttributes['fringeLocationCode'])) {
            $model->nbrptot_locn_code_frng = $positionBudget->getFringeLocationCode();
        }

        if (isset($modifiedAttributes['recurringBudgetAmount'])) {
            $model->nbrptot_recurring_budget = $positionBudget->getRecurringBudgetAmount();
        }

        if (isset($modifiedAttributes['positionBudgetBasis'])) {
            $model->nbrptot_budg_basis = $positionBudget->getPositionBudgetBasis();
        }

        if (isset($modifiedAttributes['positionAnnualBasis'])) {
            $model->nbrptot_ann_basis = $positionBudget->getPositionAnnualBasis();
        }

        if (isset($modifiedAttributes['baseUnits'])) {
            $model->nbrptot_base_units = $positionBudget->getBaseUnits();
        }

        if (isset($modifiedAttributes['appointmentPercent'])) {
            $model->nbrptot_appt_pct = $positionBudget->getAppointmentPercent();
        }

        if (isset($modifiedAttributes['createNbrjfteIndicator'])) {
            $model->nbrptot_create_jfte_ind = $positionBudget->getCreateNbrjfteIndicator();
        }

        if (isset($modifiedAttributes['comment'])) {
            $model->nbrptot_comment = $positionBudget->getComment();
        }

        $model->nbrptot_activity_date = $positionBudget->getActivityDate();
        $model->nbrptot_user_id = $positionBudget->getUserId();
        $model->nbrptot_data_origin = $positionBudget->getDataOrigin();

        $model->save();

        return $model;
    }
}
