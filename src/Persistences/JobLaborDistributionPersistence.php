<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 7/10/19
 * Time: 5:05 PM
 */

namespace MiamiOH\WSPositionBudget\Persistences;

use Illuminate\Database\Capsule\Manager as DB;
use MiamiOH\RESTng\Util\User;
use MiamiOH\WSPositionBudget\EloquentModels\AccountIndexEloquentModel;
use MiamiOH\WSPositionBudget\EloquentModels\AccountValidationEloquentModel;
use MiamiOH\WSPositionBudget\EloquentModels\FiscalYearEloquentModel;
use MiamiOH\WSPositionBudget\EloquentModels\JobLaborDistributionEloquentModel;
use MiamiOH\WSPositionBudget\Exceptions\ResourceExistsException;
use MiamiOH\WSPositionBudget\Exceptions\ResourceNotFoundException;
use MiamiOH\WSPositionBudget\Repositories\MUIDRepository;
use PDO;

/**
 * Class JobLaborDistributionPersistence
 * @package MiamiOH\WSPositionBudget\Persistences
 */
class JobLaborDistributionPersistence extends PersistenceBase
{
    /**
     * @var string
     */
    private $table = 'nbrjlbd';

    /**
     * @var MUIDRepository
     */
    private $muidRepository;

    public function __construct(MUIDRepository $muidRepository)
    {
        $this->muidRepository = $muidRepository;
    }

    /**
     * @param string $muid
     * @param string $positionNumber
     * @param string $positionSuffix
     * @param string $effectiveDate
     * @param array $data
     * @param User $user
     * @throws ResourceExistsException
     */
    public function create(
        string $muid,
        string $positionNumber,
        string $positionSuffix,
        string $effectiveDate,
        array $data,
        User $user
    ): void
    {
        $pidm = $this->muidRepository->readMUID($muid)['pidm'];
        $effectiveDateTime = new \DateTime($effectiveDate);

        if ($this->checkResourceExists($pidm, $positionNumber, $positionSuffix, $effectiveDateTime)) {
            throw new ResourceExistsException(
                "Resource with pidm $pidm" .
                ", position number $positionNumber" .
                ", position suffix $positionSuffix" .
                ", and effective date $effectiveDate" .
                " already exists."
            );
        }

        DB::beginTransaction();

        try {
            $this->insertRows(
                $pidm,
                $positionNumber,
                $positionSuffix,
                $effectiveDateTime,
                $data,
                $user
            );

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @param string $muid
     * @param string $positionNumber
     * @param string $positionSuffix
     * @param string $effectiveDate
     * @param array $data
     * @param User $user
     * @throws \Exception
     */
    public function updateOrCreate(
        string $muid,
        string $positionNumber,
        string $positionSuffix,
        string $effectiveDate,
        array $data,
        User $user
    ): void
    {
        $pidm = $this->muidRepository->readMUID($muid)['pidm'];
        $effectiveDateTime = new \DateTime($effectiveDate);

        DB::beginTransaction();

        try {
            if ($this->checkResourceExists($pidm, $positionNumber, $positionSuffix, $effectiveDateTime)) {
                $this->archiveExistingRows(
                    $pidm,
                    $positionNumber,
                    $positionSuffix,
                    $effectiveDateTime
                );
            }

            $this->insertRows(
                $pidm,
                $positionNumber,
                $positionSuffix,
                $effectiveDateTime,
                $data,
                $user
            );

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @param string $accountIndexCode
     * @return array
     * @throws \InvalidArgumentException
     */
    private function getAccountIndexCode(string $accountIndexCode): array
    {
        $record = AccountIndexEloquentModel::select('ftvacci_acci_code', 'ftvacci_fund_code', 'ftvacci_orgn_code', 'ftvacci_prog_code', 'ftvacci_locn_code', 'ftvacci_actv_code')
            ->where('ftvacci_acci_code', $accountIndexCode)
            ->where('ftvacci_coas_code', 'C')
            ->whereRaw("TO_CHAR(ftvacci_nchg_date, 'YYYYMMDD') = ?", '20991231')
            ->first();

        if (empty($record)) {
            throw new \InvalidArgumentException("Account index code $accountIndexCode is not found.");
        }

        return [
            'accountIndexCode' => $record->ftvacci_acci_code ?? '',
            'fundCode' => $record->ftvacci_fund_code ?? '',
            'orgnCode' => $record->ftvacci_orgn_code ?? '',
            'progCode' => $record->ftvacci_prog_code ?? '',
            'locationCode' => $record->ftvacci_locn_code ?? '',
            'activityCode' => $record->ftvacci_actv_code ?? ''
        ];
    }

    /**
     * @param string $accountCode
     * @return string
     * @throws \InvalidArgumentException
     */
    private function getAccountCode(string $accountCode): string
    {
        $record = AccountValidationEloquentModel::select('ftvacct_acct_code')
            ->where('ftvacct_acct_code', $accountCode)
            ->whereRaw("TO_CHAR(ftvacct_nchg_date, 'YYYYMMDD') = ?", '20991231')
            ->where('ftvacct_coas_code', 'C')
            ->first();

        if (empty($record)) {
            throw new \InvalidArgumentException("Account code $accountCode is not found.");
        }

        return $accountCode;
    }

    /**
     * @param string $pidm
     * @param string $positionNumber
     * @param string $positionSuffix
     * @param \DateTime $effectiveDateTime
     * @return bool
     * @throws \Exception
     */
    private function checkResourceExists(
        string $pidm,
        string $positionNumber,
        string $positionSuffix,
        \DateTime $effectiveDateTime
    ): bool
    {
        $formattedEffectiveDate = $effectiveDateTime->format('Ymd');

        $count = JobLaborDistributionEloquentModel::where('nbrjlbd_pidm', $pidm)
            ->where('nbrjlbd_posn', $positionNumber)
            ->where('nbrjlbd_suff', $positionSuffix)
            ->whereRaw("TO_CHAR(nbrjlbd_effective_date, 'YYYYMMDD') = ?", $formattedEffectiveDate)
            ->count();

        return $count > 0;
    }

    /**
     * @param string $chartOfCode
     * @param string $fundCode
     * @param string $organizationCode
     * @param string $accountCode
     * @param string $programCode
     * @param string $activityCode
     * @param string $locationCode
     * @param string $fiscalYear
     * @return array
     */
    private function getEncumbranceNumber(
        string $chartOfCode,
        string $fundCode,
        string $organizationCode,
        string $accountCode,
        string $programCode,
        string $activityCode,
        string $locationCode,
        string $fiscalYear
    ): array
    {

        $outEncdNum = '';
        $outEncdSeqNum = '';
        $outEncdNewInd = '';
        $outErrMesg = '';

        $pdo = DB::connection('MUWS_GEN_PROD')->getPdo();

        try {
            $stmt = $pdo->prepare(
                "call nbkjenc.p_calc_encd_num(
            :coas_code, :fund_code, :orgn_code, :acct_code,
            :prog_code, :actv_code, :locn_code, :fiscal_year,
            :out_encd_num, :out_encd_seq_num, :out_encd_new_ind, :out_err_mesg)");

            $stmt->bindValue(':coas_code', $chartOfCode, PDO::PARAM_STR, 255);
            $stmt->bindValue(':fund_code', $fundCode, PDO::PARAM_STR, 255);
            $stmt->bindValue(':orgn_code', $organizationCode, PDO::PARAM_STR, 255);
            $stmt->bindValue(':acct_code', $accountCode, PDO::PARAM_STR, 255);
            $stmt->bindValue(':prog_code', $programCode, PDO::PARAM_STR, 255);
            $stmt->bindValue(':actv_code', $activityCode, PDO::PARAM_STR, 255);
            $stmt->bindValue(':locn_code', $locationCode, PDO::PARAM_STR, 255);
            $stmt->bindValue(':fiscal_year', $fiscalYear, PDO::PARAM_STR, 255);

            $stmt->bindParam(':out_encd_num', $outEncdNum, PDO::PARAM_STR, 255);
            $stmt->bindParam(':out_encd_seq_num', $outEncdSeqNum, PDO::PARAM_STR, 255);
            $stmt->bindParam(':out_encd_new_ind', $outEncdNewInd, PDO::PARAM_STR, 255);
            $stmt->bindParam(':out_err_mesg', $outErrMesg, PDO::PARAM_STR, 255);

            $stmt->execute();
        }catch (\PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        return [
            'encumbranceNumber' => $outEncdNum,
            'encumbranceSequenceNumber' => $outEncdSeqNum
        ];
    }

    /**
     * @param \DateTime $effectiveDateTime
     * @return string
     */
    private function getFiscalYear(\DateTime $effectiveDateTime): string
    {
        $formattedEffectiveDate = $effectiveDateTime->format('Ymd');

        $record = FiscalYearEloquentModel::select('nbbfisc_code')
            ->whereRaw("TO_CHAR(nbbfisc_begin_date, 'YYYYMMDD') <= ?", $formattedEffectiveDate)
            ->whereRaw("TO_CHAR(nbbfisc_end_date, 'YYYYMMDD') >= ?", $formattedEffectiveDate)
            ->first();

        if (empty($record)) {
            throw new \InvalidArgumentException("There is no fiscal year found for $formattedEffectiveDate.");
        }

        return $record->nbbfisc_code;
    }

    /**
     * @param string $pidm
     * @param string $positionNumber
     * @param string $positionSuffix
     * @param \DateTime $effectiveDateTime
     * @param array $data
     * @param User $user
     * @param string $format
     * @throws \Exception
     */
    private function insertRows(
        string $pidm,
        string $positionNumber,
        string $positionSuffix,
        \DateTime $effectiveDateTime,
        array $data,
        User $user,
        string $format = self::ORACLE_DATE_TIME_FORMAT
    ): void
    {
        $rows = [];
        $prefix = $this->table . '_';

        $fiscalYear = $this->getFiscalYear($effectiveDateTime);
        $effectiveDateTime = $effectiveDateTime->format($format);

        foreach ($data as $row) {
            $accountIndexCode = $this->getAccountIndexCode($this->aValue($row, 'accountIndexCode'));
            $accountCode = $this->getAccountCode($this->aValue($row, 'accountCode'));
            $chartOfAccount = $this->aValue($row, 'chartOfAccountsCode', 'C');

            $encumbranceNumber = $this->getEncumbranceNumber(
                $chartOfAccount,
                $accountIndexCode['fundCode'],
                $accountIndexCode['orgnCode'],
                $accountCode,
                $accountIndexCode['progCode'],
                $accountIndexCode['activityCode'],
                $accountIndexCode['locationCode'],
                $fiscalYear
            );

            $rows[] = [
                // required
                $prefix . 'pidm' => $pidm,
                $prefix . 'posn' => $positionNumber,
                $prefix . 'suff' => $positionSuffix,
                $prefix . 'effective_date' => $effectiveDateTime,
                $prefix . 'percent' => $this->aValue($row, 'percentage'),
                $prefix . 'fringe_enc_to_post' => $this->aValue($row, 'fringeEncumbranceToPost'),
                $prefix . 'salary_enc_to_post' => $this->aValue($row, 'salaryEncumbranceToPost'),

                // non required
                $prefix . 'acct_code' => $accountCode,
                $prefix . 'acci_code' => $accountIndexCode['accountIndexCode'],
                $prefix . 'orgn_code' => $accountIndexCode['orgnCode'],
                $prefix . 'fund_code' => $accountIndexCode['fundCode'],
                $prefix . 'locn_code' => $accountIndexCode['locationCode'],
                $prefix . 'prog_code' => $accountIndexCode['progCode'],
                $prefix . 'actv_code' => $accountIndexCode['activityCode'],

                $prefix . 'coas_code' => $chartOfAccount,
                $prefix . 'ctyp_code' => $this->aValue($row, 'costTypeCode'),
                $prefix . 'proj_code' => $this->aValue($row, 'projectCode'),
                $prefix . 'change_ind' => $this->aValue($row, 'changeIndicator', 'A'),

                $prefix . 'encd_num' => $encumbranceNumber['encumbranceNumber'],
                $prefix . 'encd_seq_num' => $encumbranceNumber['encumbranceSequenceNumber'],

                $prefix . 'enc_last_calc_date' => $this->aValue($row, 'encumbranceLastCalculatedDate'),
                $prefix . 'enc_last_recast_date' => $this->aValue($row, 'encumbranceLatestRecastDate'),
                $prefix . 'enc_override_end_date' => $this->aValue($row, 'encumbranceOverrideEndDate'),
                $prefix . 'acct_code_external' => $this->aValue($row, 'externalAccountCode'),
                $prefix . 'fringe_encumbrance' => $this->aValue($row, 'fringeEncumbrance'),
                $prefix . 'fund_code_fringe' => $this->aValue($row, 'fringeFund'),
                $prefix . 'orgn_code_fringe' => $this->aValue($row, 'fringeOrganization'),
                $prefix . 'acct_code_fringe' => $this->aValue($row, 'fringeAccount'),
                $prefix . 'prog_code_fringe' => $this->aValue($row, 'fringeProgram'),
                $prefix . 'actv_code_fringe' => $this->aValue($row, 'fringeActivity'),
                $prefix . 'locn_code_fringe' => $this->aValue($row, 'fringeLocation'),
                $prefix . 'encd_num_fringe' => $this->aValue($row, 'fringeEncumbranceNumber'),
                $prefix . 'fringe_residual' => $this->aValue($row, 'fringeEncumbranceResidual'),
                $prefix . 'encd_seq_num_fringe' => $this->aValue($row, 'fringeEncumbranceSequenceNumber'),
                $prefix . 'future_fringe_residual' => $this->aValue($row, 'futureFringeEncumbranceResidual'),
                $prefix . 'fringe_res_to_post' => $this->aValue($row, 'fringeEncumbranceResidualToPost', 0),
                $prefix . 'future_frg_res_to_post' => $this->aValue($row, 'futureFringeEncumbranceResidualToPost', 0),
                $prefix . 'future_frg_enc_to_post' => $this->aValue($row, 'futureFringeEncumbranceToPost', 0),
                $prefix . 'future_sal_enc_to_post' => $this->aValue($row, 'futureSalaryEncumbranceToPost', 0),
                $prefix . 'future_fringe_enc' => $this->aValue($row, 'futureFringeEncumbrance'),
                $prefix . 'future_salary_enc' => $this->aValue($row, 'futureSalaryEncumbrance'),
                $prefix . 'salary_encumbrance' => $this->aValue($row, 'salaryEncumbrance'),

                // default fields
                $prefix . 'activity_date' => (new \DateTime('now'))->format($format),
                $prefix . 'data_origin' => 'WEB_SERVICE',
                $prefix . 'user_id' => strtoupper($this->aValue($row, 'userId', $user->getUsername()))
            ];
        }

        JobLaborDistributionEloquentModel::insert($rows);
    }

    /**
     * @param string $pidm
     * @param string $positionNumber
     * @param string $positionSuffix
     * @param \DateTime $effectiveDateTime
     */
    private function archiveExistingRows(
        string $pidm,
        string $positionNumber,
        string $positionSuffix,
        \DateTime $effectiveDateTime
    ): void
    {
        $formattedEffectiveDate = $effectiveDateTime->format('Ymd');

        JobLaborDistributionEloquentModel::where('nbrjlbd_pidm', $pidm)
            ->where('nbrjlbd_posn', $positionNumber)
            ->where('nbrjlbd_suff', $positionSuffix)
            ->whereRaw("TO_CHAR(nbrjlbd_effective_date, 'YYYYMMDD') = ?", $formattedEffectiveDate)
            ->update(['nbrjlbd_change_ind' => 'H',]);
    }
}
