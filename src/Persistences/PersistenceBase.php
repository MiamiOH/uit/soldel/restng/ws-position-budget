<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 7/12/19
 * Time: 11:28 AM
 */

namespace MiamiOH\WSPositionBudget\Persistences;


class PersistenceBase
{
    const ORACLE_DATE_TIME_FORMAT = 'Y-m-d H:i:s';

    /**
     * Return array value if array key exists. Otherwise, return default value.
     *
     * @param array $array
     * @param string $key
     * @param null $default
     * @return mixed|null
     */
    public function aValue(array $array, string $key, $default = null) {
        if (array_key_exists($key, $array)) {
            return $array[$key];
        }

        return $default;
    }
}