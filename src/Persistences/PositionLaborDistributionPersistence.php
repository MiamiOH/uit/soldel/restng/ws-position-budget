<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 7/10/19
 * Time: 5:05 PM
 */

namespace MiamiOH\WSPositionBudget\Persistences;

use Illuminate\Database\Capsule\Manager as DB;
use MiamiOH\RESTng\Util\User;
use MiamiOH\WSPositionBudget\EloquentModels\AccountIndexEloquentModel;
use MiamiOH\WSPositionBudget\EloquentModels\AccountValidationEloquentModel;
use MiamiOH\WSPositionBudget\EloquentModels\PositionBudgetEloquentModel;
use MiamiOH\WSPositionBudget\EloquentModels\PositionLaborDistributionEloquentModel;
use MiamiOH\WSPositionBudget\Exceptions\ResourceExistsException;
use MiamiOH\WSPositionBudget\Exceptions\ResourceNotFoundException;

/**
 * Class PositionLaborDistributionPersistence
 * @package MiamiOH\WSPositionBudget\Persistences
 */
class PositionLaborDistributionPersistence extends PersistenceBase
{
    /**
     * @var string
     */
    private $prefix = 'nbrplbd_';

    /**
     * @param string $positionNumber
     * @param int $fiscalYear
     * @param array $data
     * @param User $user
     * @param string $format
     * @throws \Exception
     */
    public function create(string $positionNumber, int $fiscalYear, array $data, User $user, string $format = self::ORACLE_DATE_TIME_FORMAT): void
    {
        if ($this->checkResourceExists($positionNumber, $fiscalYear)) {
            throw new ResourceExistsException("Resource with position number $positionNumber and fiscal year $fiscalYear already exists.");
        }

        DB::beginTransaction();

        try {
            $this->insertRows($positionNumber, $fiscalYear, $data, $user, $format);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @param string $positionNumber
     * @param int $fiscalYear
     * @param array $data
     * @param User $user
     * @param string $format
     * @throws \Exception
     */
    public function updateOrCreate(string $positionNumber, int $fiscalYear, array $data, User $user, string $format = self::ORACLE_DATE_TIME_FORMAT): void
    {
        DB::beginTransaction();

        try {
            if ($this->checkResourceExists($positionNumber, $fiscalYear)) {
                $this->archiveExistingRows($positionNumber, $fiscalYear);
            }

            $this->insertRows($positionNumber, $fiscalYear, $data, $user, $format);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @param string $accountIndexCode
     * @return array
     * @throws \InvalidArgumentException
     */
    private function getAccountIndexCode(string $accountIndexCode): array
    {
        $record = AccountIndexEloquentModel::select('ftvacci_acci_code', 'ftvacci_fund_code', 'ftvacci_orgn_code', 'ftvacci_prog_code', 'ftvacci_locn_code', 'ftvacci_actv_code')
            ->where('ftvacci_acci_code', $accountIndexCode)
            ->whereRaw("TO_CHAR(ftvacci_nchg_date, 'DD-MON-YYYY') = ?", '31-DEC-2099')
            ->first();


        if (empty($record)) {
            throw new \InvalidArgumentException("Account index code $accountIndexCode is not found.");
        }

        return [
            'accountIndexCode' => $record->ftvacci_acci_code,
            'fundCode' => $record->ftvacci_fund_code,
            'orgnCode' => $record->ftvacci_orgn_code,
            'progCode' => $record->ftvacci_prog_code,
            'locationCode' => $record->ftvacci_locn_code,
            'activityCode' => $record->ftvacci_actv_code
        ];

    }


    /**
     * @param string $accountCode
     * @return string
     * @throws \InvalidArgumentException
     */
    private function getAccountCode(string $accountCode): string
    {
        $record = AccountValidationEloquentModel::select('ftvacct_acct_code')
            ->where('ftvacct_acct_code', $accountCode)
            ->whereRaw("TO_CHAR(ftvacct_nchg_date, 'DD-MON-YYYY') = ?", '31-DEC-2099')
            ->where('ftvacct_coas_code', 'C')
            ->first();

        if (empty($record)) {
            throw new \InvalidArgumentException("Account code $accountCode is not found.");
        }
        return $accountCode;
    }


    /**
     * @param string $positionNumber
     * @param int $fiscalYear
     * @return array|null
     */
    private function getBudgetInfo(string $positionNumber, int $fiscalYear): ?array
    {
        $record = PositionBudgetEloquentModel::select('nbrptot_obph_code', 'nbrptot_obud_code')
            ->where('nbrptot_posn', $positionNumber)
            ->where('nbrptot_fisc_code', $fiscalYear)
            ->first();

        if (empty($record)) {
            throw new \InvalidArgumentException("There is no budget information for position number $positionNumber and fiscal year $fiscalYear.");
        }

        return [
            'budgetIdCode' => $record->nbrptot_obud_code ?? '',
            'budgetPhaseCode' => $record->nbrptot_obph_code ?? '',
        ];
    }


    /**
     * @param string $positionNumber
     * @param int $fiscalYear
     * @return bool
     */
    private function checkResourceExists(string $positionNumber, int $fiscalYear): bool
    {
        $count = PositionLaborDistributionEloquentModel::where('nbrplbd_posn', $positionNumber)
            ->where('nbrplbd_fisc_code', $fiscalYear)
            ->count();

        return $count > 0;
    }


    /**
     * @param string $positionNumber
     * @param int $fiscalYear
     * @param array $data
     * @param User $user
     * @param string $format
     * @param array $rows
     * @throws \Exception
     */
    private function insertRows(string $positionNumber, int $fiscalYear, array $data, User $user, string $format): void
    {
        $rows = [];
        $budgetInfo = $this->getBudgetInfo($positionNumber, $fiscalYear);

        foreach ($data as $row) {
            $accountIndexCode = $this->getAccountIndexCode($this->aValue($row, 'accountIndexCode'));
            $accountCode = $this->getAccountCode($this->aValue($row, 'accountCode'));

            $rows[] = [
                // required
                $this->prefix . 'posn' => $positionNumber,
                $this->prefix . 'fisc_code' => $fiscalYear,
                $this->prefix . 'percent' => $this->aValue($row, 'percentage'),

                // non required
                $this->prefix . 'acct_code' => $accountCode,
                $this->prefix . 'acci_code' => $accountIndexCode['accountIndexCode'],
                $this->prefix . 'orgn_code' => $accountIndexCode['orgnCode'],
                $this->prefix . 'fund_code' => $accountIndexCode['fundCode'],
                $this->prefix . 'locn_code' => $accountIndexCode['locationCode'],
                $this->prefix . 'prog_code' => $accountIndexCode['progCode'],
                $this->prefix . 'actv_code' => $accountIndexCode['activityCode'],

                $this->prefix . 'obud_code' => $this->aValue($row, 'budgetIdCode', $budgetInfo['budgetIdCode']),
                $this->prefix . 'obph_code' => $this->aValue($row, 'budgetPhaseCode', $budgetInfo['budgetPhaseCode']),
                $this->prefix . 'budget' => $this->aValue($row, 'budget', 0),
                $this->prefix . 'budget_to_post' => $this->aValue($row, 'budgetToPost', 0),
                $this->prefix . 'change_ind' => $this->aValue($row, 'changeIndicator'),
                $this->prefix . 'coas_code' => $this->aValue($row, 'chartOfAccountsCode', 'C'),
                $this->prefix . 'ctyp_code' => $this->aValue($row, 'costTypeCode'),
                $this->prefix . 'acct_code_external' => $this->aValue($row, 'externalAccountCode'),
                $this->prefix . 'proj_code' => $this->aValue($row, 'projectCode'),

                $this->prefix . 'activity_date' => (new \DateTime('now'))->format($format),
                $this->prefix . 'data_origin' => 'WEB_SERVICE',
                $this->prefix . 'user_id' => strtoupper($this->aValue($row, 'userId', $user->getUsername()))
            ];
        }

        PositionLaborDistributionEloquentModel::insert($rows);
    }

    /**
     * @param string $positionNumber
     * @param int $fiscalYear
     */
    private function archiveExistingRows(string $positionNumber, int $fiscalYear): void
    {
        PositionLaborDistributionEloquentModel::where('nbrplbd_posn', $positionNumber)
            ->where('nbrplbd_fisc_code', $fiscalYear)
            ->update([
                $this->prefix . 'budget_to_post' => DB::raw("nbrplbd_budget * -1"),
                $this->prefix . 'change_ind' => 'D',
                $this->prefix . 'percent' => 0,
                $this->prefix . 'budget' => 0
            ]);
    }
}
