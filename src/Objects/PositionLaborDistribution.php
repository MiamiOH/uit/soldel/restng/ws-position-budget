<?php

namespace MiamiOH\WSPositionBudget\Objects;

use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;

/**
 * Class PositionLaborDistribution
 * @package MiamiOH\WSPositionBudget\Objects
 */
class PositionLaborDistribution
{
    const ORACLE_DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * @var string
     */
    private $fiscalYearCode = '';
    /**
     * @var float
     */
    private $percentage = 0.0;
    /**
     * @var string
     */
    private $position = '';

    /**
     * @var string
     */
    private $accountCode = '';
    /**
     * @var string
     */
    private $accountIndexCode = '';
    /**
     * @var string
     */
    private $activityCode = '';
    /**
     * @var float
     */
    private $budget = 0.0;
    /**
     * @var string
     */
    private $budgetIdCode = '';
    /**
     * @var string
     */
    private $budgetPhaseCode = '';
    /**
     * @var float
     */
    private $budgetToPost = 0.0;
    /**
     * @var string
     */
    private $changeIndicator = '';
    /**
     * @var string
     */
    private $chartOfAccountsCode = '';
    /**
     * @var string
     */
    private $costTypeCode = '';
    /**
     * @var string
     */
    private $externalAccountCode = '';
    /**
     * @var string
     */
    private $fundCode = '';
    /**
     * @var string
     */
    private $locationCode = '';
    /**
     * @var string
     */
    private $organizationCode = '';
    /**
     * @var string
     */
    private $programCode = '';
    /**
     * @var string
     */
    private $projectCode = '';
    /**
     * @var string
     */
    private $dataOrigin = '';
    /**
     * @var string
     */
    private $userId = '';
    /**
     * @var string
     */
    private $surrogateId = '';

    /**
     * @return string
     */
    public function getDataOrigin(): string
    {
        return $this->dataOrigin;
    }

    /**
     * @param string $dataOrigin
     * @return PositionLaborDistribution
     */
    public function setDataOrigin(string $dataOrigin): PositionLaborDistribution
    {
        $this->dataOrigin = $dataOrigin;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     * @return JobLaborDistribution
     */
    public function setUserId(string $userId): PositionLaborDistribution
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getPosition(): string
    {
        return $this->position;
    }

    /**
     * @param string $position
     * @return JobLaborDistribution
     */
    public function setPosition(string $position): PositionLaborDistribution
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return string
     */
    public function getChartOfAccountsCode(): string
    {
        return $this->chartOfAccountsCode;
    }

    /**
     * @param string $chartOfAccountsCode
     * @return PositionLaborDistribution
     */
    public function setChartOfAccountsCode(string $chartOfAccountsCode): PositionLaborDistribution
    {
        $this->chartOfAccountsCode = $chartOfAccountsCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountIndexCode(): string
    {
        return $this->accountIndexCode;
    }

    /**
     * @param string $accountIndexCode
     * @return PositionLaborDistribution
     */
    public function setAccountIndexCode(string $accountIndexCode): PositionLaborDistribution
    {
        $this->accountIndexCode = $accountIndexCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getFundCode(): string
    {
        return $this->fundCode;
    }

    /**
     * @param string $fundCode
     * @return PositionLaborDistribution
     */
    public function setFundCode(string $fundCode): PositionLaborDistribution
    {
        $this->fundCode = $fundCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrganizationCode(): string
    {
        return $this->organizationCode;
    }

    /**
     * @param string $organizationCode
     * @return PositionLaborDistribution
     */
    public function setOrganizationCode(string $organizationCode): PositionLaborDistribution
    {
        $this->organizationCode = $organizationCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountCode(): string
    {
        return $this->accountCode;
    }

    /**
     * @param string $accountCode
     * @return PositionLaborDistribution
     */
    public function setAccountCode(string $accountCode): PositionLaborDistribution
    {
        $this->accountCode = $accountCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getProgramCode(): string
    {
        return $this->programCode;
    }

    /**
     * @param string $programCode
     * @return PositionLaborDistribution
     */
    public function setProgramCode(string $programCode): PositionLaborDistribution
    {
        $this->programCode = $programCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getActivityCode(): string
    {
        return $this->activityCode;
    }

    /**
     * @param string $activityCode
     * @return PositionLaborDistribution
     */
    public function setActivityCode(string $activityCode): PositionLaborDistribution
    {
        $this->activityCode = $activityCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocationCode(): string
    {
        return $this->locationCode;
    }

    /**
     * @param string $locationCode
     * @return PositionLaborDistribution
     */
    public function setLocationCode(string $locationCode): PositionLaborDistribution
    {
        $this->locationCode = $locationCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getProjectCode(): string
    {
        return $this->projectCode;
    }

    /**
     * @param string $projectCode
     * @return PositionLaborDistribution
     */
    public function setProjectCode(string $projectCode): PositionLaborDistribution
    {
        $this->projectCode = $projectCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getCostTypeCode(): string
    {
        return $this->costTypeCode;
    }

    /**
     * @param string $costTypeCode
     * @return PositionLaborDistribution
     */
    public function setCostTypeCode(string $costTypeCode): PositionLaborDistribution
    {
        $this->costTypeCode = $costTypeCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getExternalAccountCode(): string
    {
        return $this->externalAccountCode;
    }

    /**
     * @param string $externalAccountCode
     * @return PositionLaborDistribution
     */
    public function setExternalAccountCode(string $externalAccountCode): PositionLaborDistribution
    {
        $this->externalAccountCode = $externalAccountCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getPercentage(): string
    {
        return $this->percentage;
    }

    /**
     * @param string $percentage
     * @return PositionLaborDistribution
     */
    public function setPercentage(string $percentage): PositionLaborDistribution
    {
        $this->percentage = $percentage;
        return $this;
    }

    /**
     * @return string
     */
    public function getChangeIndicator(): string
    {
        return $this->changeIndicator;
    }

    /**
     * @param string $changeIndicator
     * @return PositionLaborDistribution
     */
    public function setChangeIndicator(string $changeIndicator): PositionLaborDistribution
    {
        $this->changeIndicator = $changeIndicator;
        return $this;
    }

    /**
     * @param string $format
     * @return string
     */
    public function getActivityDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        return (new \DateTime('now'))->format($format);
    }

    /**
     * @return string
     */
    public function getFiscalYearCode(): string
    {
        return $this->fiscalYearCode;
    }

    /**
     * @param string $fiscalYearCode
     * @return PositionLaborDistribution
     */
    public function setFiscalYearCode(string $fiscalYearCode): PositionLaborDistribution
    {
        $this->fiscalYearCode = $fiscalYearCode;
        return $this;
    }

    /**
     * @return float
     */
    public function getBudget(): float
    {
        return $this->budget;
    }

    /**
     * @param float $budget
     * @return PositionLaborDistribution
     */
    public function setBudget(float $budget): PositionLaborDistribution
    {
        $this->budget = $budget;
        return $this;
    }

    /**
     * @return string
     */
    public function getBudgetIdCode(): string
    {
        return $this->budgetIdCode;
    }

    /**
     * @param string $budgetIdCode
     * @return PositionLaborDistribution
     */
    public function setBudgetIdCode(string $budgetIdCode): PositionLaborDistribution
    {
        $this->budgetIdCode = $budgetIdCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getBudgetPhaseCode(): string
    {
        return $this->budgetPhaseCode;
    }

    /**
     * @param string $budgetPhaseCode
     * @return PositionLaborDistribution
     */
    public function setBudgetPhaseCode(string $budgetPhaseCode): PositionLaborDistribution
    {
        $this->budgetPhaseCode = $budgetPhaseCode;
        return $this;
    }

    /**
     * @return float
     */
    public function getBudgetToPost(): float
    {
        return $this->budgetToPost;
    }

    /**
     * @param float $budgetToPost
     * @return PositionLaborDistribution
     */
    public function setBudgetToPost(float $budgetToPost): PositionLaborDistribution
    {
        $this->budgetToPost = $budgetToPost;
        return $this;
    }

    /**
     * @return string
     */
    public function getSurrogateId(): string
    {
        return $this->surrogateId;
    }

    /**
     * @param string $surrogateId
     * @return PositionLaborDistribution
     */
    public function setSurrogateId(string $surrogateId): PositionLaborDistribution
    {
        $this->surrogateId = $surrogateId;
        return $this;
    }


    private static $rules = [
        //required fields
        'fiscalYearCode' => ['required', 'max:4'],
        'percentage' => ['required', 'numeric'],
        'position' => ['required', 'max:6'],

        //non required fields
        'accountCode' => ['max:6'],
        'accountIndexCode' => ['max:6'],
        'activityCode' => ['max:6'],
        'budget' => ['numeric'],
        'budgetIdCode' => ['max:6'],
        'budgetPhaseCode' => ['max:6'],
        'budgetToPost' => ['numeric'],
        'changeIndicator' => ['max:1'],
        'chartOfAccountsCode' => ['max:1'],
        'costTypeCode' => ['max:2'],
        'externalAccountCode' => ['max:60'],
        'fundCode' => ['max:6'],
        'locationCode' => ['max:6'],
        'organizationCode' => ['max:6'],
        'programCode' => ['max:6'],
        'projectCode' => ['max:8'],

        // non user-defined field
        'dataOrigin' => ['max:30'],
        'userId' => ['max:30']
    ];

    private static $updateRules = [
        'fiscalYearCode' => ['max:4'],
        'percentage' => ['numeric'],
        'position' => ['max:6'],
        'accountCode' => ['max:6'],
        'accountIndexCode' => ['max:6'],
        'activityCode' => ['max:6'],
        'budget' => ['numeric'],
        'budgetIdCode' => ['max:6'],
        'budgetPhaseCode' => ['max:6'],
        'budgetToPost' => ['numeric'],
        'changeIndicator' => ['max:1'],
        'chartOfAccountsCode' => ['max:1'],
        'costTypeCode' => ['max:2'],
        'externalAccountCode' => ['max:60'],
        'fundCode' => ['max:6'],
        'locationCode' => ['max:6'],
        'organizationCode' => ['max:6'],
        'programCode' => ['max:6'],
        'projectCode' => ['max:8'],

        // non user-defined fields
        'dataOrigin' => ['max:30'],
        'userId' => ['max:30']
    ];

    private $modifiedAttributes = [];

    /**
     * @return array
     */
    public function getModifiedAttributes(): array
    {
        return $this->modifiedAttributes;
    }

    /**
     * @param string $attributes
     */
    public function addModifiedAttributes(string $attributes): void
    {
        $this->modifiedAttributes[$attributes] = true;
    }

    /**
     * @param array $data
     * @return PositionLaborDistribution
     * @throws \Exception
     */
    public static function fromArray(array $data): self
    {
        self::validateArray($data);

        $positionLaborDistribution = new self();

        try {
            foreach ($data as $key => $val) {
                $positionLaborDistribution->{'set' . ucfirst($key)}(strtoupper($val));
            }
        } catch (\Exception $e) {
            throw new \InvalidArgumentException("Invalid key for Position Labor Distribution: $key. " . $e->getMessage());
        }

        return $positionLaborDistribution;
    }

    public static function fromUpdate(array $data): self
    {
        self::validateUpdate($data);

        $positionLaborDistribution = new self();

        try {
            foreach ($data as $key => $val) {
                $positionLaborDistribution->{'set' . ucfirst($key)}(strtoupper($val));
                $positionLaborDistribution->addModifiedAttributes($key);
            }
        } catch (\Exception $e) {
            throw new \InvalidArgumentException("Invalid key for Position Labor Distribution: $key. " . $e->getMessage());
        }

        return $positionLaborDistribution;
    }

    /**
     * @param array $data
     * @throws \Exception
     */
    public static function validateArray(array $data): void
    {
        $validator = RESTngValidatorFactory::make($data, self::$rules);

        if ($validator->fails()) {
            throw new \InvalidArgumentException(
                'Validation failed. '
                . implode(' ', $validator->errors()->all())
            );
        }
    }

    public static function validateUpdate(array $data): void
    {
        $validator = RESTngValidatorFactory::make($data, self::$updateRules);

        if ($validator->fails()) {
            throw new \InvalidArgumentException(
                'Validation failed. '
                . implode(' ', $validator->errors()->all())
            );
        }
    }
}
