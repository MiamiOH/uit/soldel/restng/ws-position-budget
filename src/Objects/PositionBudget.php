<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 12/1/18
 * Time: 8:39 PM
 */

namespace MiamiOH\WSPositionBudget\Objects;

use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;
use function var_dump;

/**
 * Class PositionBudget
 * @package MiamiOH\WSPositionBudget\Objects
 */
class PositionBudget
{
    const ORACLE_DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * @var string
     */
    private $position = '';
    /**
     * @var string
     */
    private $fiscalYearCode = '';
    /**
     * @var \DateTime
     */
    private $effectiveDate = null;
    /**
     * @var string
     */
    private $status = '';
    /**
     * @var string
     */
    private $fullTimeEquivalency = '';
    /**
     * @var string
     */
    private $chartOfAccountsCode = '';
    /**
     * @var string
     */
    private $organizationCode = '';
    /**
     * @var string
     */
    private $budget = '';
    /**
     * @var string
     */
    private $encumbranceAmount = '';
    /**
     * @var string
     */
    private $expenditureAmount = '';
    /**
     * @var string
     */
    private $type = '';
    /**
     * @var string
     */
    private $budgetIdCode = '';
    /**
     * @var string
     */
    private $budgetPhaseCode = '';
    /**
     * @var string
     */
    private $salaryGroupCode = '';
    /**
     * @var string
     */
    private $accountIndexCode = '';
    /**
     * @var string
     */
    private $positionBudgetFringe = '';
    /**
     * @var string
     */
    private $fringeEncumbrance = '';
    /**
     * @var string
     */
    private $fringeExpenditure = '';
    /**
     * @var string
     */
    private $fringeFundCode = '';
    /**
     * @var string
     */
    private $fringeOrganizationCode = '';
    /**
     * @var string
     */
    private $fringeAccountCode = '';
    /**
     * @var string
     */
    private $fringeProgramCode = '';
    /**
     * @var string
     */
    private $fringeActivityCode = '';
    /**
     * @var string
     */
    private $fringeLocationCode = '';
    /**
     * @var string
     */
    private $recurringBudgetAmount = '';
    /**
     * @var string
     */
    private $positionBudgetBasis = '';
    /**
     * @var string
     */
    private $positionAnnualBasis = '';
    /**
     * @var string
     */
    private $baseUnits = '';
    /**
     * @var string
     */
    private $appointmentPercent = '';
    /**
     * @var string
     */
    private $createNbrjfteIndicator = '';
    /**
     * @var string
     */
    private $dataOrigin = '';
    /**
     * @var string
     */
    private $userId = '';
    /**
     * @var string
     */
    private $comment = '';

    /**
     * @param array $data
     * @return PositionBudget
     */
    private static function setFields(array $data): PositionBudget
    {
        $positionBudget = new self();

        try {
            foreach ($data as $key => $val) {
                $positionBudget->{'set' . ucfirst($key)}(strtoupper($val));
                $positionBudget->addModifiedAttributes($key);
            }
        } catch (\Exception $e) {
            throw new \InvalidArgumentException("Invalid key for Position Budget: $key" . $e->getMessage());
        }
        return $positionBudget;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return PositionBudget
     */
    public function setComment(string $comment): PositionBudget
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     */
    public function setUserId(string $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getDataOrigin(): string
    {
        return $this->dataOrigin;
    }

    /**
     * @param string $dataOrigin
     */
    public function setDataOrigin(string $dataOrigin): void
    {
        $this->dataOrigin = $dataOrigin;
    }

    /**
     * @return string
     */
    public function getPosition(): string
    {
        return $this->position;
    }

    /**
     * @param string $position
     * @return PositionBudget
     */
    public function setPosition(string $position): PositionBudget
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return string
     */
    public function getFiscalYearCode(): string
    {
        return $this->fiscalYearCode;
    }

    /**
     * @param string $fiscalYearCode
     * @return PositionBudget
     */
    public function setFiscalYearCode(string $fiscalYearCode): PositionBudget
    {
        $this->fiscalYearCode = $fiscalYearCode;
        return $this;
    }

    /**
     * @param string $format
     * @return string
     */
    public function getActivityDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        return (new \DateTime('now'))->format($format);
    }

    /**
     * @param string $format
     * @return string
     */
    public function getEffectiveDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        if (empty($this->effectiveDate)) {
            return '';
        }

        return $this->effectiveDate->format($format);
    }

    /**
     * @param string $effectiveDate
     * @return PositionBudget
     */
    public function setEffectiveDate(string $effectiveDate): PositionBudget
    {
        if (empty($effectiveDate)) {
            $this->effectiveDate = null;
        } else {
            $this->effectiveDate = new \DateTime($effectiveDate);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return PositionBudget
     */
    public function setStatus(string $status): PositionBudget
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullTimeEquivalency(): string
    {
        return $this->fullTimeEquivalency;
    }

    /**
     * @param string $fullTimeEquivalency
     * @return PositionBudget
     */
    public function setFullTimeEquivalency(string $fullTimeEquivalency): PositionBudget
    {
        $this->fullTimeEquivalency = $fullTimeEquivalency;
        return $this;
    }

    /**
     * @return string
     */
    public function getChartOfAccountsCode(): string
    {
        return $this->chartOfAccountsCode;
    }

    /**
     * @param string $chartOfAccountsCode
     * @return PositionBudget
     */
    public function setChartOfAccountsCode(string $chartOfAccountsCode): PositionBudget
    {
        $this->chartOfAccountsCode = $chartOfAccountsCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrganizationCode(): string
    {
        return $this->organizationCode;
    }

    /**
     * @param string $organizationCode
     * @return PositionBudget
     */
    public function setOrganizationCode(string $organizationCode): PositionBudget
    {
        $this->organizationCode = $organizationCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getBudget(): string
    {
        return $this->budget;
    }

    /**
     * @param string $budget
     * @return PositionBudget
     */
    public function setBudget(string $budget): PositionBudget
    {
        $this->budget = $budget;
        return $this;
    }

    /**
     * @return string
     */
    public function getEncumbranceAmount(): string
    {
        return $this->encumbranceAmount;
    }

    /**
     * @param string $encumbranceAmount
     * @return PositionBudget
     */
    public function setEncumbranceAmount(string $encumbranceAmount): PositionBudget
    {
        $this->encumbranceAmount = $encumbranceAmount;
        return $this;
    }

    /**
     * @return string
     */
    public function getExpenditureAmount(): string
    {
        return $this->expenditureAmount;
    }

    /**
     * @param string $expenditureAmount
     * @return PositionBudget
     */
    public function setExpenditureAmount(string $expenditureAmount): PositionBudget
    {
        $this->expenditureAmount = $expenditureAmount;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return PositionBudget
     */
    public function setType(string $type): PositionBudget
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getBudgetIdCode(): string
    {
        return $this->budgetIdCode;
    }

    /**
     * @param string $budgetIdCode
     * @return PositionBudget
     */
    public function setBudgetIdCode(string $budgetIdCode): PositionBudget
    {
        $this->budgetIdCode = $budgetIdCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getBudgetPhaseCode(): string
    {
        return $this->budgetPhaseCode;
    }

    /**
     * @param string $budgetPhaseCode
     * @return PositionBudget
     */
    public function setBudgetPhaseCode(string $budgetPhaseCode): PositionBudget
    {
        $this->budgetPhaseCode = $budgetPhaseCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getSalaryGroupCode(): string
    {
        return $this->salaryGroupCode;
    }

    /**
     * @param string $salaryGroupCode
     * @return PositionBudget
     */
    public function setSalaryGroupCode(string $salaryGroupCode): PositionBudget
    {
        $this->salaryGroupCode = $salaryGroupCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountIndexCode(): string
    {
        return $this->accountIndexCode;
    }

    /**
     * @param string $accountIndexCode
     * @return PositionBudget
     */
    public function setAccountIndexCode(string $accountIndexCode): PositionBudget
    {
        $this->accountIndexCode = $accountIndexCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getPositionBudgetFringe(): string
    {
        return $this->positionBudgetFringe;
    }

    /**
     * @param string $positionBudgetFringe
     * @return PositionBudget
     */
    public function setPositionBudgetFringe(string $positionBudgetFringe): PositionBudget
    {
        $this->positionBudgetFringe = $positionBudgetFringe;
        return $this;
    }

    /**
     * @return string
     */
    public function getFringeEncumbrance(): string
    {
        return $this->fringeEncumbrance;
    }

    /**
     * @param string $fringeEncumbrance
     * @return PositionBudget
     */
    public function setFringeEncumbrance(string $fringeEncumbrance): PositionBudget
    {
        $this->fringeEncumbrance = $fringeEncumbrance;
        return $this;
    }

    /**
     * @return string
     */
    public function getFringeExpenditure(): string
    {
        return $this->fringeExpenditure;
    }

    /**
     * @param string $fringeExpenditure
     * @return PositionBudget
     */
    public function setFringeExpenditure(string $fringeExpenditure): PositionBudget
    {
        $this->fringeExpenditure = $fringeExpenditure;
        return $this;
    }

    /**
     * @return string
     */
    public function getFringeFundCode(): string
    {
        return $this->fringeFundCode;
    }

    /**
     * @param string $fringeFundCode
     * @return PositionBudget
     */
    public function setFringeFundCode(string $fringeFundCode): PositionBudget
    {
        $this->fringeFundCode = $fringeFundCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getFringeOrganizationCode(): string
    {
        return $this->fringeOrganizationCode;
    }

    /**
     * @param string $fringeOrganizationCode
     * @return PositionBudget
     */
    public function setFringeOrganizationCode(string $fringeOrganizationCode): PositionBudget
    {
        $this->fringeOrganizationCode = $fringeOrganizationCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getFringeAccountCode(): string
    {
        return $this->fringeAccountCode;
    }

    /**
     * @param string $fringeAccountCode
     * @return PositionBudget
     */
    public function setFringeAccountCode(string $fringeAccountCode): PositionBudget
    {
        $this->fringeAccountCode = $fringeAccountCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getFringeProgramCode(): string
    {
        return $this->fringeProgramCode;
    }

    /**
     * @param string $fringeProgramCode
     * @return PositionBudget
     */
    public function setFringeProgramCode(string $fringeProgramCode): PositionBudget
    {
        $this->fringeProgramCode = $fringeProgramCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getFringeActivityCode(): string
    {
        return $this->fringeActivityCode;
    }

    /**
     * @param string $fringeActivityCode
     * @return PositionBudget
     */
    public function setFringeActivityCode(string $fringeActivityCode): PositionBudget
    {
        $this->fringeActivityCode = $fringeActivityCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getFringeLocationCode(): string
    {
        return $this->fringeLocationCode;
    }

    /**
     * @param string $fringeLocationCode
     * @return PositionBudget
     */
    public function setFringeLocationCode(string $fringeLocationCode): PositionBudget
    {
        $this->fringeLocationCode = $fringeLocationCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getRecurringBudgetAmount(): string
    {
        return $this->recurringBudgetAmount;
    }

    /**
     * @param string $recurringBudgetAmount
     * @return PositionBudget
     */
    public function setRecurringBudgetAmount(string $recurringBudgetAmount): PositionBudget
    {
        $this->recurringBudgetAmount = $recurringBudgetAmount;
        return $this;
    }

    /**
     * @return string
     */
    public function getPositionBudgetBasis(): string
    {
        return $this->positionBudgetBasis;
    }

    /**
     * @param string $positionBudgetBasis
     * @return PositionBudget
     */
    public function setPositionBudgetBasis(string $positionBudgetBasis): PositionBudget
    {
        $this->positionBudgetBasis = $positionBudgetBasis;
        return $this;
    }

    /**
     * @return string
     */
    public function getPositionAnnualBasis(): string
    {
        return $this->positionAnnualBasis;
    }

    /**
     * @param string $positionAnnualBasis
     * @return PositionBudget
     */
    public function setPositionAnnualBasis(string $positionAnnualBasis): PositionBudget
    {
        $this->positionAnnualBasis = $positionAnnualBasis;
        return $this;
    }

    /**
     * @return string
     */
    public function getBaseUnits(): string
    {
        return $this->baseUnits;
    }

    /**
     * @param string $baseUnits
     * @return PositionBudget
     */
    public function setBaseUnits(string $baseUnits): PositionBudget
    {
        $this->baseUnits = $baseUnits;
        return $this;
    }

    /**
     * @return string
     */
    public function getAppointmentPercent(): string
    {
        return $this->appointmentPercent;
    }

    /**
     * @param string $appointmentPercent
     * @return PositionBudget
     */
    public function setAppointmentPercent(string $appointmentPercent): PositionBudget
    {
        $this->appointmentPercent = $appointmentPercent;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreateNbrjfteIndicator(): string
    {
        return $this->createNbrjfteIndicator;
    }

    /**
     * @param string $createNbrjfteIndicator
     */
    public function setCreateNbrjfteIndicator(string $createNbrjfteIndicator): void
    {
        $this->createNbrjfteIndicator = $createNbrjfteIndicator;
    }

    private $modifiedAttributes = [];

    /**
     * @return array
     */
    public function getModifiedAttributes(): array
    {
        return $this->modifiedAttributes;
    }

    /**
     * @param string $attributes
     */
    public function addModifiedAttributes(string $attributes): void
    {
        $this->modifiedAttributes[$attributes] = true;
    }

    private static $requiredRules = [
        // required fields
        'appointmentPercent' => ['required', 'numeric', 'regex:/^\d{1,3}(\.\d*)?$/'],
        'baseUnits' => ['required', 'numeric', 'regex:/^\d{1,3}(\.\d*)?$/'],
        'createNbrjfteIndicator' => ['required', 'max:1'],
        'effectiveDate' => ['required', 'date'],
        'fiscalYearCode' => ['required', 'max:4'],
        'fullTimeEquivalency' => ['required', 'numeric', 'regex:/^\d{1,3}(\.\d*)?$/'],
        'organizationCode' => ['required', 'max:6'],
        'position' => ['required', 'max:6'],
        'positionAnnualBasis' => ['required', 'numeric', 'regex:/^\d{1,4}(\.\d*)?$/'],
        'positionBudgetBasis' => ['required', 'numeric', 'regex:/^\d{1,4}(\.\d*)?$/']
        ];

    private static $nonRequiredRules = [
        // non required fields
        'accountIndexCode' => ['max:6'],
        'budget' => ['numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'budgetIdCode' => ['max:6'],
        'budgetPhaseCode' => ['max:6'],
        'chartOfAccountsCode' => ['max:1'],
        'comment' => ['max:2000'],
        'encumbranceAmount' => ['numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'expenditureAmount' => ['numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'fringeAccountCode' => ['max:6'],
        'fringeActivityCode' => ['max:6'],
        'fringeEncumbrance' => ['numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'fringeExpenditure' => ['numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'fringeFundCode' => ['max:6'],
        'fringeLocationCode' => ['max:6'],
        'fringeOrganizationCode' => ['max:6'],
        'fringeProgramCode' => ['max:6'],
        'positionBudgetFringe' => ['numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'salaryGroupCode' => ['max:6'],
        'status' => ['max:1'],
        'recurringBudgetAmount' => ['numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],

        // non user-defined field
        'dataOrigin' => ['max:30'],
        'userId' => ['max:30']
    ];

    private static $updateRules = [
        'appointmentPercent' => ['numeric', 'regex:/^\d{1,3}(\.\d*)?$/'],
        'baseUnits' => ['numeric', 'regex:/^\d{1,3}(\.\d*)?$/'],
        'createNbrjfteIndicator' => ['max:1'],
        'effectiveDate' => ['date'],
        'fiscalYearCode' => ['max:4'],
        'fullTimeEquivalency' => ['numeric', 'regex:/^\d{1,3}(\.\d*)?$/'],
        'organizationCode' => ['max:6'],
        'positionAnnualBasis' => ['numeric', 'regex:/^\d{1,4}(\.\d*)?$/'],
        'positionBudgetBasis' => ['numeric', 'regex:/^\d{1,4}(\.\d*)?$/']
        ];

    /**
     * @param array $data
     * @return PositionBudget
     * @throws \Exception
     */
    public static function fromPostArray(array $data): self
    {
        self::validateArray(self::$requiredRules, self::$nonRequiredRules, $data);
        return self::setFields($data);
    }

    public static function fromPutArray(array $data): self
    {
        self::validateArray(self::$updateRules, self::$nonRequiredRules, $data);
        return self::setFields($data);
    }

    /**
     * @param array $requiredRules
     * @param array $nonRequiredRules
     * @param array $data
     */
    public static function validateArray(array $requiredRules, array $nonRequiredRules, array $data): void
    {
        $rules = array_merge($requiredRules, $nonRequiredRules);
        $validator = RESTngValidatorFactory::make($data, $rules);

        if ($validator->fails()) {
            throw new \InvalidArgumentException(
                'Validation failed. '
                . implode(' ', $validator->errors()->all())
            );
        }
    }
}
