<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 12/5/18
 * Time: 2:34 PM
 */

namespace MiamiOH\WSPositionBudget\Objects;

use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;

/**
 * Class JobLaborDistribution
 * @package MiamiOH\WSPositionBudget\Objects
 */
class JobLaborDistribution
{
    const ORACLE_DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * @var string
     */
    private $muid = '';
    /**
     * @var string
     */
    private $position = '';
    /**
     * @var string
     */
    private $suffix = '';
    /**
     * @var string
     */
    private $chartOfAccountsCode = '';
    /**
     * @var string
     */
    private $accountIndexCode = '';
    /**
     * @var string
     */
    private $fundCode = '';
    /**
     * @var string
     */
    private $organizationCode = '';
    /**
     * @var string
     */
    private $accountCode = '';
    /**
     * @var string
     */
    private $programCode = '';
    /**
     * @var string
     */
    private $activityCode = '';
    /**
     * @var string
     */
    private $locationCode = '';
    /**
     * @var string
     */
    private $projectCode = '';
    /**
     * @var string
     */
    private $costTypeCode = '';
    /**
     * @var string
     */
    private $externalAccountCode = '';
    /**
     * @var string
     */
    private $percentage = '';
    /**
     * @var string
     */
    private $encumbranceNumber = '';
    /**
     * @var string
     */
    private $encumbranceSequenceNumber = '';
    /**
     * @var string
     */
    private $salaryEncumbrance = '';
    /**
     * @var string
     */
    private $salaryEncumbranceToPost = '';
    /**
     * @var string
     */
    private $fringeEncumbrance = '';
    /**
     * @var string
     */
    private $fringeEncumbranceToPost = '';
    /**
     * @var string
     */
    private $fringeFund = '';
    /**
     * @var string
     */
    private $fringeOrganization = '';
    /**
     * @var string
     */
    private $fringeAccount = '';
    /**
     * @var string
     */
    private $fringeProgram = '';
    /**
     * @var string
     */
    private $fringeActivity = '';
    /**
     * @var string
     */
    private $fringeLocation = '';
    /**
     * @var string
     */
    private $changeIndicator = '';
    /**
     * @var string
     */
    private $fringeEncumbranceResidual = '';
    /**
     * @var string
     */
    private $fringeEncumbranceResidualToPost = '';
    /**
     * @var string
     */
    private $futureSalaryEncumbrance = '';
    /**
     * @var string
     */
    private $futureFringeEncumbrance = '';
    /**
     * @var string
     */
    private $futureFringeEncumbranceResidual = '';
    /**
     * @var string
     */
    private $futureSalaryEncumbranceToPost = '';
    /**
     * @var string
     */
    private $futureFringeEncumbranceToPost = '';
    /**
     * @var string
     */
    private $futureFringeEncumbranceResidualToPost = '';
    /**
     * @var \DateTime
     */
    private $effectiveDate = null;
    /**
     * @var \DateTime
     */
    private $encumbranceOverrideEndDate = null;
    /**
     * @var \DateTime
     */
    private $encumbranceLatestRecastDate = null;
    /**
     * @var \DateTime
     */
    private $encumbranceLastCalculatedDate = null;
    /**
     * @var string
     */
    private $fringeEncumbranceNumber = '';
    /**
     * @var string
     */
    private $fringeEncumbranceSequenceNumber = '';
    /**
     * @var string
     */
    private $dataOrigin = '';
    /**
     * @var string
     */
    private $userId = '';
    /**
     * @var string
     */
    private $surrogateId= '';
    /**
     * @var string
     */
    private $pidm= '';

    /**
     * @return string
     */
    public function getPidm(): string
    {
        return $this->pidm;
    }

    /**
     * @param string $pidm
     * @return JobLaborDistribution
     */
    public function setPidm(string $pidm): JobLaborDistribution
    {
        $this->pidm = $pidm;
        return $this;
    }

    /**
     * @return string
     */
    public function getSurrogateId(): string
    {
        return $this->surrogateId;
    }

    /**
     * @param string $surrogateId
     * @return JobLaborDistribution
     */
    public function setSurrogateId(string $surrogateId): JobLaborDistribution
    {
        $this->surrogateId = $surrogateId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDataOrigin(): string
    {
        return $this->dataOrigin;
    }

    /**
     * @param string $dataOrigin
     * @return JobLaborDistribution
     */
    public function setDataOrigin(string $dataOrigin): JobLaborDistribution
    {
        $this->dataOrigin = $dataOrigin;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     * @return JobLaborDistribution
     */
    public function setUserId(string $userId): JobLaborDistribution
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @param string $format
     * @return string
     */
    public function getActivityDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        return (new \DateTime('now'))->format($format);
    }

    /**
     * @param string $format
     * @return string
     */
    public function getEffectiveDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        if (empty($this->effectiveDate)) {
            return '';
        }

        return $this->effectiveDate->format($format);
    }

    /**
     * @param string $effectiveDate
     * @return JobLaborDistribution
     */
    public function setEffectiveDate(string $effectiveDate): JobLaborDistribution
    {
        if (empty($effectiveDate)) {
            $this->effectiveDate = null;
        } else {
            $this->effectiveDate = new \DateTime($effectiveDate);
        }

        return $this;
    }

    /**
     * @param string $format
     * @return string
     */
    public function getEncumbranceOverrideEndDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        if (empty($this->encumbranceOverrideEndDate)) {
            return '';
        }

        return $this->encumbranceOverrideEndDate->format($format);
    }

    /**
     * @param string $encumbranceOverrideEndDate
     * @return JobLaborDistribution
     */
    public function setEncumbranceOverrideEndDate(string $encumbranceOverrideEndDate): JobLaborDistribution
    {
        if (empty($encumbranceOverrideEndDate)) {
            $this->encumbranceOverrideEndDate = null;
        } else {
            $this->encumbranceOverrideEndDate = new \DateTime($encumbranceOverrideEndDate);
        }

        return $this;
    }

    /**
     * @param string $format
     * @return string
     */
    public function getEncumbranceLatestRecastDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        if (empty($this->encumbranceLatestRecastDate)) {
            return '';
        }

        return $this->encumbranceLatestRecastDate->format($format);
    }

    /**
     * @param string $encumbranceLatestRecastDate
     * @return JobLaborDistribution
     */
    public function setEncumbranceLatestRecastDate(string $encumbranceLatestRecastDate): JobLaborDistribution
    {
        if (empty($encumbranceLatestRecastDate)) {
            $this->encumbranceLatestRecastDate = null;
        } else {
            $this->encumbranceLatestRecastDate = new \DateTime($encumbranceLatestRecastDate);
        }

        return $this;
    }

    /**
     * @param string $format
     * @return string
     */
    public function getEncumbranceLastCalculatedDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        if (empty($this->encumbranceLastCalculatedDate)) {
            return '';
        }

        return $this->encumbranceLastCalculatedDate->format($format);
    }

    /**
     * @param string $encumbranceLastCalculatedDate
     * @return JobLaborDistribution
     */
    public function setEncumbranceLastCalculatedDate(string $encumbranceLastCalculatedDate): JobLaborDistribution
    {
        if (empty($encumbranceLastCalculatedDate)) {
            $this->encumbranceLastCalculatedDate = null;
        } else {
            $this->encumbranceLastCalculatedDate = new \DateTime($encumbranceLastCalculatedDate);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getMuid(): string
    {
        return $this->muid;
    }

    /**
     * @param string $muid
     * @return JobLaborDistribution
     */
    public function setMuid(string $muid): JobLaborDistribution
    {
        $this->muid = $muid;
        return $this;
    }

    /**
     * @return string
     */
    public function getPosition(): string
    {
        return $this->position;
    }

    /**
     * @param string $position
     * @return JobLaborDistribution
     */
    public function setPosition(string $position): JobLaborDistribution
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return string
     */
    public function getSuffix(): string
    {
        return $this->suffix;
    }

    /**
     * @param string $suffix
     * @return JobLaborDistribution
     */
    public function setSuffix(string $suffix): JobLaborDistribution
    {
        $this->suffix = $suffix;
        return $this;
    }

    /**
     * @return string
     */
    public function getChartOfAccountsCode(): string
    {
        return $this->chartOfAccountsCode;
    }

    /**
     * @param string $chartOfAccountsCode
     * @return JobLaborDistribution
     */
    public function setChartOfAccountsCode(string $chartOfAccountsCode): JobLaborDistribution
    {
        $this->chartOfAccountsCode = $chartOfAccountsCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountIndexCode(): string
    {
        return $this->accountIndexCode;
    }

    /**
     * @param string $accountIndexCode
     * @return JobLaborDistribution
     */
    public function setAccountIndexCode(string $accountIndexCode): JobLaborDistribution
    {
        $this->accountIndexCode = $accountIndexCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getFundCode(): string
    {
        return $this->fundCode;
    }

    /**
     * @param string $fundCode
     * @return JobLaborDistribution
     */
    public function setFundCode(string $fundCode): JobLaborDistribution
    {
        $this->fundCode = $fundCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrganizationCode(): string
    {
        return $this->organizationCode;
    }

    /**
     * @param string $organizationCode
     * @return JobLaborDistribution
     */
    public function setOrganizationCode(string $organizationCode): JobLaborDistribution
    {
        $this->organizationCode = $organizationCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountCode(): string
    {
        return $this->accountCode;
    }

    /**
     * @param string $accountCode
     * @return JobLaborDistribution
     */
    public function setAccountCode(string $accountCode): JobLaborDistribution
    {
        $this->accountCode = $accountCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getProgramCode(): string
    {
        return $this->programCode;
    }

    /**
     * @param string $programCode
     * @return JobLaborDistribution
     */
    public function setProgramCode(string $programCode): JobLaborDistribution
    {
        $this->programCode = $programCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getActivityCode(): string
    {
        return $this->activityCode;
    }

    /**
     * @param string $activityCode
     * @return JobLaborDistribution
     */
    public function setActivityCode(string $activityCode): JobLaborDistribution
    {
        $this->activityCode = $activityCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocationCode(): string
    {
        return $this->locationCode;
    }

    /**
     * @param string $locationCode
     * @return JobLaborDistribution
     */
    public function setLocationCode(string $locationCode): JobLaborDistribution
    {
        $this->locationCode = $locationCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getProjectCode(): string
    {
        return $this->projectCode;
    }

    /**
     * @param string $projectCode
     * @return JobLaborDistribution
     */
    public function setProjectCode(string $projectCode): JobLaborDistribution
    {
        $this->projectCode = $projectCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getCostTypeCode(): string
    {
        return $this->costTypeCode;
    }

    /**
     * @param string $costTypeCode
     * @return JobLaborDistribution
     */
    public function setCostTypeCode(string $costTypeCode): JobLaborDistribution
    {
        $this->costTypeCode = $costTypeCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getExternalAccountCode(): string
    {
        return $this->externalAccountCode;
    }

    /**
     * @param string $externalAccountCode
     * @return JobLaborDistribution
     */
    public function setExternalAccountCode(string $externalAccountCode): JobLaborDistribution
    {
        $this->externalAccountCode = $externalAccountCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getPercentage(): string
    {
        return $this->percentage;
    }

    /**
     * @param string $percentage
     * @return JobLaborDistribution
     */
    public function setPercentage(string $percentage): JobLaborDistribution
    {
        $this->percentage = $percentage;
        return $this;
    }

    /**
     * @return string
     */
    public function getEncumbranceNumber(): string
    {
        return $this->encumbranceNumber;
    }

    /**
     * @param string $encumbranceNumber
     * @return JobLaborDistribution
     */
    public function setEncumbranceNumber(string $encumbranceNumber): JobLaborDistribution
    {
        $this->encumbranceNumber = $encumbranceNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getEncumbranceSequenceNumber(): string
    {
        return $this->encumbranceSequenceNumber;
    }

    /**
     * @param string $encumbranceSequenceNumber
     * @return JobLaborDistribution
     */
    public function setEncumbranceSequenceNumber(string $encumbranceSequenceNumber): JobLaborDistribution
    {
        $this->encumbranceSequenceNumber = $encumbranceSequenceNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getSalaryEncumbrance(): string
    {
        return $this->salaryEncumbrance;
    }

    /**
     * @param string $salaryEncumbrance
     * @return JobLaborDistribution
     */
    public function setSalaryEncumbrance(string $salaryEncumbrance): JobLaborDistribution
    {
        $this->salaryEncumbrance = $salaryEncumbrance;
        return $this;
    }

    /**
     * @return string
     */
    public function getSalaryEncumbranceToPost(): string
    {
        return $this->salaryEncumbranceToPost;
    }

    /**
     * @param string $salaryEncumbranceToPost
     * @return JobLaborDistribution
     */
    public function setSalaryEncumbranceToPost(string $salaryEncumbranceToPost): JobLaborDistribution
    {
        $this->salaryEncumbranceToPost = $salaryEncumbranceToPost;
        return $this;
    }

    /**
     * @return string
     */
    public function getFringeEncumbrance(): string
    {
        return $this->fringeEncumbrance;
    }

    /**
     * @param string $fringeEncumbrance
     * @return JobLaborDistribution
     */
    public function setFringeEncumbrance(string $fringeEncumbrance): JobLaborDistribution
    {
        $this->fringeEncumbrance = $fringeEncumbrance;
        return $this;
    }

    /**
     * @return string
     */
    public function getFringeEncumbranceToPost(): string
    {
        return $this->fringeEncumbranceToPost;
    }

    /**
     * @param string $fringeEncumbranceToPost
     * @return JobLaborDistribution
     */
    public function setFringeEncumbranceToPost(string $fringeEncumbranceToPost): JobLaborDistribution
    {
        $this->fringeEncumbranceToPost = $fringeEncumbranceToPost;
        return $this;
    }

    /**
     * @return string
     */
    public function getFringeFund(): string
    {
        return $this->fringeFund;
    }

    /**
     * @param string $fringeFund
     * @return JobLaborDistribution
     */
    public function setFringeFund(string $fringeFund): JobLaborDistribution
    {
        $this->fringeFund = $fringeFund;
        return $this;
    }

    /**
     * @return string
     */
    public function getFringeOrganization(): string
    {
        return $this->fringeOrganization;
    }

    /**
     * @param string $fringeOrganization
     * @return JobLaborDistribution
     */
    public function setFringeOrganization(string $fringeOrganization): JobLaborDistribution
    {
        $this->fringeOrganization = $fringeOrganization;
        return $this;
    }

    /**
     * @return string
     */
    public function getFringeAccount(): string
    {
        return $this->fringeAccount;
    }

    /**
     * @param string $fringeAccount
     * @return JobLaborDistribution
     */
    public function setFringeAccount(string $fringeAccount): JobLaborDistribution
    {
        $this->fringeAccount = $fringeAccount;
        return $this;
    }

    /**
     * @return string
     */
    public function getFringeProgram(): string
    {
        return $this->fringeProgram;
    }

    /**
     * @param string $fringeProgram
     * @return JobLaborDistribution
     */
    public function setFringeProgram(string $fringeProgram): JobLaborDistribution
    {
        $this->fringeProgram = $fringeProgram;
        return $this;
    }

    /**
     * @return string
     */
    public function getFringeActivity(): string
    {
        return $this->fringeActivity;
    }

    /**
     * @param string $fringeActivity
     * @return JobLaborDistribution
     */
    public function setFringeActivity(string $fringeActivity): JobLaborDistribution
    {
        $this->fringeActivity = $fringeActivity;
        return $this;
    }

    /**
     * @return string
     */
    public function getFringeLocation(): string
    {
        return $this->fringeLocation;
    }

    /**
     * @param string $fringeLocation
     * @return JobLaborDistribution
     */
    public function setFringeLocation(string $fringeLocation): JobLaborDistribution
    {
        $this->fringeLocation = $fringeLocation;
        return $this;
    }

    /**
     * @return string
     */
    public function getChangeIndicator(): string
    {
        return $this->changeIndicator;
    }

    /**
     * @param string $changeIndicator
     * @return JobLaborDistribution
     */
    public function setChangeIndicator(string $changeIndicator): JobLaborDistribution
    {
        $this->changeIndicator = $changeIndicator;
        return $this;
    }

    /**
     * @return string
     */
    public function getFringeEncumbranceResidual(): string
    {
        return $this->fringeEncumbranceResidual;
    }

    /**
     * @param string $fringeEncumbranceResidual
     * @return JobLaborDistribution
     */
    public function setFringeEncumbranceResidual(string $fringeEncumbranceResidual): JobLaborDistribution
    {
        $this->fringeEncumbranceResidual = $fringeEncumbranceResidual;
        return $this;
    }

    /**
     * @return string
     */
    public function getFringeEncumbranceResidualToPost(): string
    {
        return $this->fringeEncumbranceResidualToPost;
    }

    /**
     * @param string $fringeEncumbranceResidualToPost
     * @return JobLaborDistribution
     */
    public function setFringeEncumbranceResidualToPost(string $fringeEncumbranceResidualToPost): JobLaborDistribution
    {
        $this->fringeEncumbranceResidualToPost = $fringeEncumbranceResidualToPost;
        return $this;
    }

    /**
     * @return string
     */
    public function getFutureSalaryEncumbrance(): string
    {
        return $this->futureSalaryEncumbrance;
    }

    /**
     * @param string $futureSalaryEncumbrance
     * @return JobLaborDistribution
     */
    public function setFutureSalaryEncumbrance(string $futureSalaryEncumbrance): JobLaborDistribution
    {
        $this->futureSalaryEncumbrance = $futureSalaryEncumbrance;
        return $this;
    }

    /**
     * @return string
     */
    public function getFutureFringeEncumbrance(): string
    {
        return $this->futureFringeEncumbrance;
    }

    /**
     * @param string $futureFringeEncumbrance
     * @return JobLaborDistribution
     */
    public function setFutureFringeEncumbrance(string $futureFringeEncumbrance): JobLaborDistribution
    {
        $this->futureFringeEncumbrance = $futureFringeEncumbrance;
        return $this;
    }

    /**
     * @return string
     */
    public function getFutureFringeEncumbranceResidual(): string
    {
        return $this->futureFringeEncumbranceResidual;
    }

    /**
     * @param string $futureFringeEncumbranceResidual
     * @return JobLaborDistribution
     */
    public function setFutureFringeEncumbranceResidual(string $futureFringeEncumbranceResidual): JobLaborDistribution
    {
        $this->futureFringeEncumbranceResidual = $futureFringeEncumbranceResidual;
        return $this;
    }

    /**
     * @return string
     */
    public function getFutureSalaryEncumbranceToPost(): string
    {
        return $this->futureSalaryEncumbranceToPost;
    }

    /**
     * @param string $futureSalaryEncumbranceToPost
     * @return JobLaborDistribution
     */
    public function setFutureSalaryEncumbranceToPost(string $futureSalaryEncumbranceToPost): JobLaborDistribution
    {
        $this->futureSalaryEncumbranceToPost = $futureSalaryEncumbranceToPost;
        return $this;
    }

    /**
     * @return string
     */
    public function getFutureFringeEncumbranceToPost(): string
    {
        return $this->futureFringeEncumbranceToPost;
    }

    /**
     * @param string $futureFringeEncumbranceToPost
     * @return JobLaborDistribution
     */
    public function setFutureFringeEncumbranceToPost(string $futureFringeEncumbranceToPost): JobLaborDistribution
    {
        $this->futureFringeEncumbranceToPost = $futureFringeEncumbranceToPost;
        return $this;
    }

    /**
     * @return string
     */
    public function getFutureFringeEncumbranceResidualToPost(): string
    {
        return $this->futureFringeEncumbranceResidualToPost;
    }

    /**
     * @param string $futureFringeEncumbranceResidualToPost
     * @return JobLaborDistribution
     */
    public function setFutureFringeEncumbranceResidualToPost(string $futureFringeEncumbranceResidualToPost): JobLaborDistribution
    {
        $this->futureFringeEncumbranceResidualToPost = $futureFringeEncumbranceResidualToPost;
        return $this;
    }

    /**
     * @return string
     */
    public function getFringeEncumbranceNumber(): string
    {
        return $this->fringeEncumbranceNumber;
    }

    /**
     * @param string $fringeEncumbranceNumber
     * @return JobLaborDistribution
     */
    public function setFringeEncumbranceNumber(string $fringeEncumbranceNumber): JobLaborDistribution
    {
        $this->fringeEncumbranceNumber = $fringeEncumbranceNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getFringeEncumbranceSequenceNumber(): string
    {
        return $this->fringeEncumbranceSequenceNumber;
    }

    /**
     * @param string $fringeEncumbranceSequenceNumber
     * @return JobLaborDistribution
     */
    public function setFringeEncumbranceSequenceNumber(string $fringeEncumbranceSequenceNumber): JobLaborDistribution
    {
        $this->fringeEncumbranceSequenceNumber = $fringeEncumbranceSequenceNumber;
        return $this;
    }

    private static $rules = [
        // required fields
        'changeIndicator' => ['required', 'max:1'],
        'effectiveDate' => ['required', 'date'],
        'fringeEncumbranceToPost' => ['required', 'numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'muid' => ['required'], // delegate validation to ws-muid
        'percentage' => ['required', 'numeric', 'regex:/^\d{1,3}(\.\d*)?$/'],
        'position' => ['required', 'max:6'],
        'salaryEncumbranceToPost' => ['required', 'numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'suffix' => ['required', 'max:2'],

        // non required fields
        'accountCode' => ['max:6'],
        'accountIndexCode' => ['max:6'],
        'activityCode' => ['max:6'],
        'chartOfAccountsCode' => ['max:1'],
        'costTypeCode' => ['max:2'],
        'encumbranceNumber' => ['max:8'],
        'encumbranceLastCalculatedDate' => ['date'],
        'encumbranceLatestRecastDate' => ['date'],
        'encumbranceOverrideEndDate' => ['date'],
        'encumbranceSequenceNumber' => ['numeric', 'regex:/^\d{1,4}$/'],
        'externalAccountCode' => ['max:60'],
        'fringeEncumbrance' => ['numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'fringeFund' => ['max:6'],
        'fringeOrganization' => ['max:6'],
        'fringeAccount' => ['max:6'],
        'fringeProgram' => ['max:6'],
        'fringeActivity' => ['max:6'],
        'fringeLocation' => ['max:6'],
        'fringeEncumbranceNumber' => ['max:8'],
        'fringeEncumbranceResidual' => ['numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'fringeEncumbranceResidualToPost' => ['numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'fringeEncumbranceSequenceNumber' => ['numeric', 'regex:/^\d{1,4}$/'],
        'futureFringeEncumbrance' => ['numeric', 'regex:/^\d{1,15}(\.\d*)?$/'],
        'futureFringeEncumbranceResidual' => ['numeric', 'regex:/^\d{1,15}(\.\d*)?$/'],
        'futureFringeEncumbranceResidualToPost' => ['numeric', 'regex:/^\d{1,15}(\.\d*)?$/'],
        'futureFringeEncumbranceToPost' => ['numeric', 'regex:/^\d{1,15}(\.\d*)?$/'],
        'futureSalaryEncumbrance' => ['numeric', 'regex:/^\d{1,15}(\.\d*)?$/'],
        'futureSalaryEncumbranceToPost' => ['numeric', 'regex:/^\d{1,15}(\.\d*)?$/'],
        'fundCode' => ['max:6'],
        'locationCode' => ['max:6'],
        'organizationCode' => ['max:6'],
        'programCode' => ['max:6'],
        'projectCode' => ['max:8'],
        'salaryEncumbrance' => ['numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],

        // non user-defined field
        'dataOrigin' => ['max:30'],
        'userId' => ['max:30']
    ];

    private static $updateRules = [
        'changeIndicator' => ['max:1'],
        'effectiveDate' => ['date'],
        'fringeEncumbranceToPost' => ['numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'muid' => ['regex:/^(\w{1,8}|\d{1,8}|\+[0-9]{1,8})$/'], // delegate validation to ws-muid
        'percentage' => ['numeric', 'regex:/^\d{1,3}(\.\d*)?$/'],
        'position' => ['max:6'],
        'salaryEncumbranceToPost' => ['numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'suffix' => ['max:2'],
        'accountCode' => ['max:6'],
        'accountIndexCode' => ['max:6'],
        'activityCode' => ['max:6'],
        'chartOfAccountsCode' => ['max:1'],
        'costTypeCode' => ['max:2'],
        'encumbranceNumber' => ['max:8'],
        'encumbranceLastCalculatedDate' => ['date'],
        'encumbranceLatestRecastDate' => ['date'],
        'encumbranceOverrideEndDate' => ['date'],
        'encumbranceSequenceNumber' => ['numeric', 'regex:/^\d{1,4}$/'],
        'externalAccountCode' => ['max:60'],
        'fringeEncumbrance' => ['numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'fringeFund' => ['max:6'],
        'fringeOrganization' => ['max:6'],
        'fringeAccount' => ['max:6'],
        'fringeProgram' => ['max:6'],
        'fringeActivity' => ['max:6'],
        'fringeLocation' => ['max:6'],
        'fringeEncumbranceNumber' => ['max:8'],
        'fringeEncumbranceResidual' => ['numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'fringeEncumbranceResidualToPost' => ['numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'fringeEncumbranceSequenceNumber' => ['numeric', 'regex:/^\d{1,4}$/'],
        'futureFringeEncumbrance' => ['numeric', 'regex:/^\d{1,15}(\.\d*)?$/'],
        'futureFringeEncumbranceResidual' => ['numeric', 'regex:/^\d{1,15}(\.\d*)?$/'],
        'futureFringeEncumbranceResidualToPost' => ['numeric', 'regex:/^\d{1,15}(\.\d*)?$/'],
        'futureFringeEncumbranceToPost' => ['numeric', 'regex:/^\d{1,15}(\.\d*)?$/'],
        'futureSalaryEncumbrance' => ['numeric', 'regex:/^\d{1,15}(\.\d*)?$/'],
        'futureSalaryEncumbranceToPost' => ['numeric', 'regex:/^\d{1,15}(\.\d*)?$/'],
        'fundCode' => ['max:6'],
        'locationCode' => ['max:6'],
        'organizationCode' => ['max:6'],
        'programCode' => ['max:6'],
        'projectCode' => ['max:8'],
        'salaryEncumbrance' => ['numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],

        // non user-defined field
        'dataOrigin' => ['max:30'],
        'userId' => ['max:30']
    ];


    private $modifiedAttributes = [];

    /**
     * @return array
     */
    public function getModifiedAttributes(): array
    {
        return $this->modifiedAttributes;
    }

    /**
     * @param string $attributes
     */
    public function addModifiedAttributes(string $attributes): void
    {
        $this->modifiedAttributes[$attributes] = true;
    }

    /**
     * @param array $data
     * @return JobLaborDistribution
     * @throws \Exception
     */
    public static function fromArray(array $data): self
    {
        self::validateArray($data);

        $jobLaborDistribution = new self();

        try {
            foreach ($data as $key => $val) {
                $jobLaborDistribution->{'set' . ucfirst($key)}(strtoupper($val));
            }
        } catch (\Exception $e) {
            throw new \InvalidArgumentException("Invalid key for Job Labor Distribution: $key. " . $e->getMessage());
        }

        return $jobLaborDistribution;
    }

    /**
     * @param array $data
     * @return JobLaborDistribution
     * @throws \Exception
     */
    public static function fromUpdate(array $data): self
    {
        self::validateUpdate($data);

        $jobLaborDistribution = new self();

        try {
            foreach ($data as $key => $val) {
                $jobLaborDistribution->{'set' . ucfirst($key)}(strtoupper($val));
                $jobLaborDistribution->addModifiedAttributes($key);
            }
        } catch (\Exception $e) {
            throw new \InvalidArgumentException("Invalid key for Job Labor Distribution: $key. " . $e->getMessage());
        }

        return $jobLaborDistribution;
    }


    /**
     * @param array $data
     * @throws \Exception
     */
    public static function validateArray(array $data): void
    {
        $validator = RESTngValidatorFactory::make($data, self::$rules);

        if ($validator->fails()) {
            throw new \InvalidArgumentException(
                'Validation failed. '
                . implode(' ', $validator->errors()->all())
            );
        }
    }

    public static function validateUpdate(array $data): void
    {
        $validator = RESTngValidatorFactory::make($data, self::$updateRules);

        if ($validator->fails()) {
            throw new \InvalidArgumentException(
                'Validation failed. '
                . implode(' ', $validator->errors()->all())
            );
        }
    }
}
