<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 2019-05-15
 * Time: 14:28
 */

namespace MiamiOH\WSPositionBudget\Services\JobLaborDistribution;

use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;
use MiamiOH\WSPositionBudget\EloquentModels\JobLaborDistributionEloquentModel;
use MiamiOH\WSPositionBudget\Repositories\JobLaborDistributionRepositorySQL;
use MiamiOH\WSPositionBudget\Repositories\JobLaborDistributionRepository;

class Get
{
    private $rules = [
        'muid' => ['required', 'regex:/^(\w{1,8}|\d{1,8}|\+[0-9]{1,8})$/']
    ];

    /**
     * @param Request $request
     * @param Response $response
     * @param JobLaborDistributionRepository $repository
     * @return Response
     * @throws \MiamiOH\RESTng\Exception\BadRequest
     */
    public function getJobLaborDistribution(
        Request $request,
        Response $response,
        JobLaborDistributionRepository $repository
    )
    {
        $status = \MiamiOH\RESTng\App::API_OK;

        $options = $request->getOptions();

        $muid = $options['muid'] ?? '';

        $suffix = $options['suffix'] ?? '';

        $position = $options['position'] ?? '';

        //Validate input parameters
        $muid = trim($muid);
        $suffix = trim($suffix);
        $position = trim($position);

        $validator = RESTngValidatorFactory::make(
            [
                'muid' => $muid,
                'suffix' => $suffix,
                'position' => $position
            ],
            $this->rules
        );

        if ($validator->fails()) {
            $response->setPayload([
                $validator->errors()->getMessages()
            ]);
            $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);

            return $response;
        }

        if (empty($options['muid'])){
            throw new \MiamiOH\RESTng\Exception\BadRequest('Please provide correct muid');
        }

        if (empty($options['suffix'])){
            throw new \MiamiOH\RESTng\Exception\BadRequest('Please provide correct suffix');
        }

        if (empty($options['position'])){
            throw new \MiamiOH\RESTng\Exception\BadRequest('Please provide correct position');
        }

        $payload = $repository->read($muid, $suffix, $position);

        if (empty($payload)) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }

        $response->setTotalObjects(count($payload));
        $response->setPayload($payload);
        $response->setStatus($status);

        return $response;

    }

}
