<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 2019-05-17
 * Time: 11:10
 */

namespace MiamiOH\WSPositionBudget\Services\JobLaborDistribution;

use MiamiOH\RESTng\Util\User;
use MiamiOH\WSPositionBudget\Exceptions\MUIDNotFoundException;
use MiamiOH\WSPositionBudget\Exceptions\ResourceNotFoundException;
use MiamiOH\WSPositionBudget\Objects\JobLaborDistribution;
use MiamiOH\WSPositionBudget\Repositories\JobLaborDistributionRepository;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\App;
use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory as Validator;
use MiamiOH\WSPositionBudget\Persistences\JobLaborDistributionPersistence;

/**
 * Class Put
 * @package MiamiOH\RestngPersonWebService\Services\Identification
 */
class Put
{
    private static $bodyRules = [
        // required fields
        'percentage' => ['required', 'numeric', 'regex:/^\d{1,3}(\.\d*)?$/'],
        'changeIndicator' => ['required', 'max:1'],
        'fringeEncumbranceToPost' => ['required', 'numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'salaryEncumbranceToPost' => ['required', 'numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'accountCode' => ['required', 'max:6'],
        'accountIndexCode' => ['required', 'max:6'],

        // non required fields
        'projectCode' => ['max:8'],
        'chartOfAccountsCode' => ['max:1'],
        'costTypeCode' => ['max:2'],
        'encumbranceNumber' => ['max:8'],
        'encumbranceLastCalculatedDate' => ['date'],
        'encumbranceLatestRecastDate' => ['date'],
        'encumbranceOverrideEndDate' => ['date'],
        'encumbranceSequenceNumber' => ['numeric', 'regex:/^\d{1,4}$/'],
        'externalAccountCode' => ['max:60'],
        'fringeEncumbrance' => ['numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'fringeFund' => ['max:6'],
        'fringeOrganization' => ['max:6'],
        'fringeAccount' => ['max:6'],
        'fringeProgram' => ['max:6'],
        'fringeActivity' => ['max:6'],
        'fringeLocation' => ['max:6'],
        'fringeEncumbranceNumber' => ['max:8'],
        'fringeEncumbranceResidual' => ['numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'fringeEncumbranceResidualToPost' => ['numeric', 'regex:/^\d{1,9}(\.\d*)?$/'],
        'fringeEncumbranceSequenceNumber' => ['numeric', 'regex:/^\d{1,4}$/'],
        'futureFringeEncumbrance' => ['numeric', 'regex:/^\d{1,15}(\.\d*)?$/'],
        'futureFringeEncumbranceResidual' => ['numeric', 'regex:/^\d{1,15}(\.\d*)?$/'],
        'futureFringeEncumbranceResidualToPost' => ['numeric', 'regex:/^\d{1,15}(\.\d*)?$/'],
        'futureFringeEncumbranceToPost' => ['numeric', 'regex:/^\d{1,15}(\.\d*)?$/'],
        'futureSalaryEncumbrance' => ['numeric', 'regex:/^\d{1,15}(\.\d*)?$/'],
        'futureSalaryEncumbranceToPost' => ['numeric', 'regex:/^\d{1,15}(\.\d*)?$/'],
        'salaryEncumbrance' => ['numeric', 'regex:/^\d{1,9}(\.\d*)?$/']
    ];

    private static $paramRules = [
        'muid' => ['required', 'regex:/^(\w{1,8}|\d{1,8}|\+[0-9]{1,8})$/'],
        'positionNumber' => ['required', 'max:6'],
        'positionSuffix' => ['required', 'numeric', 'regex:/^\d{0,2}$/'],
        'effectiveDate' => ['required', 'date']
    ];

    /**
     * @param Request $request
     * @param Response $response
     * @param User $user
     * @param JobLaborDistributionRepository $repository
     * @return Response
     * @throws \Exception
     */
    public function putSingle(
        Request $request,
        Response $response,
        User $user,
        JobLaborDistributionRepository $repository
    ): Response
    {
        $status = \MiamiOH\RESTng\App::API_CREATED;

        $data = $request->getData();
        $surrogateId = $request->getResourceParam('surrogateId');

        if (empty($data)) {
            $payload['errors'][] = 'No Data';
            $status = \MiamiOH\RESTng\App::API_BADREQUEST;

            $response->setPayload($payload);
            $response->setStatus($status);
            return $response;
        }

        $data['userId'] = $user->getUsername();
        $data['dataOrigin'] = 'WebService';
        $data['surrogateId'] = $surrogateId;

        try {
            $laborDistribution = JobLaborDistribution::fromUpdate($data);
            $repository->update($laborDistribution);
        } catch (\Exception $e) {
            $response->setPayload([$e->getMessage()]);
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
            return $response;
        }

        // DONE
        $response->setPayload(['1 record updated']);
        $response->setStatus($status);
        return $response;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param User $user
     * @param JobLaborDistributionPersistence $persistence
     * @return Response
     * @throws \Exception
     */
    public function putCollection(
        Request $request,
        Response $response,
        User $user,
        JobLaborDistributionPersistence $persistence
    ): Response
    {
        $data = $request->getData();

        $muid = $request->getResourceParam('muid');
        $positionNumber = $request->getResourceParam('positionNumber');
        $positionSuffix = $request->getResourceParam('positionSuffix');
        $effectiveDate = $request->getResourceParam('effectiveDate');

        $payload = [];

        if (empty($data)) {
            $payload['error']['message'] = 'No Data.';

            $response->setPayload($payload);
            $response->setStatus(App::API_BADREQUEST);
            return $response;
        }

        $errors = $this->validateInput($muid, $positionNumber, $positionSuffix, $effectiveDate, $data);

        if (!empty($errors)) {
            $payload['error']['message'] = implode(' ', $errors);

            $response->setPayload($payload);
            $response->setStatus(App::API_BADREQUEST);
            return $response;
        }

        try {
            $persistence->updateOrCreate($muid, $positionNumber, $positionSuffix, $effectiveDate, $data, $user);
        } catch (\Exception $e) {
            $payload['error']['message'] = $e->getMessage();
            $response->setPayload($payload);

            if ($e instanceof ResourceNotFoundException) {
                $response->setStatus(App::API_NOTFOUND);
            } elseif ($e instanceof \InvalidArgumentException || $e instanceof MUIDNotFoundException) {
                $response->setStatus(App::API_BADREQUEST);
            } else {
                $response->setStatus(App::API_FAILED);
            }

            return $response;
        }

        // DONE
        $response->setPayload(['Records successfully updated.']);
        $response->setStatus(App::API_CREATED);
        return $response;
    }

    private function validateInput(string $muid, string $positionNumber, string $positionSuffix, string $effectiveDate, array $data): array
    {
        $errors = [];
        $totalPercentage = 0;

        $validator = Validator::make(
            [
                'muid' => $muid,
                'positionNumber' => $positionNumber,
                'positionSuffix' => $positionSuffix,
                'effectiveDate' => $effectiveDate
            ],
            self::$paramRules);

        if ($validator->fails()) {
            $errors[] = "Validation failed. " . implode(' ', $validator->errors()->all());
        }

        // Validate Input
        foreach ($data as $index => $row) {
            $rowNumber = $index + 1;
            $validator = Validator::make($row, self::$bodyRules);

            if ($validator->fails()) {
                $errors[] = "Row $rowNumber: Validation failed. " . implode(' ', $validator->errors()->all());
                continue;
            }

            $totalPercentage += floatval($row['percentage']);
        }

        if (empty($errors) && $totalPercentage != 100) {
            $errors[] = "Total percentage should be 100; provided total percentage is $totalPercentage.";
        }

        return $errors;
    }
}