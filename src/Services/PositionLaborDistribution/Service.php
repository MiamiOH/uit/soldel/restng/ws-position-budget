<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 9/24/18
 * Time: 3:05 PM
 */

namespace MiamiOH\WSPositionBudget\Services\JobLaborDistribution;

use MiamiOH\WSPositionBudget\Persistences\JobLaborDistributionPersistence;
use MiamiOH\WSPositionBudget\Repositories\JobLaborDistributionRepository;
use MiamiOH\WSPositionBudget\Repositories\JobLaborDistributionRepositorySQL;
use MiamiOH\RESTngIlluminateIntegration\RESTngEloquentFactory;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use MiamiOH\RESTng\Legacy\DataSource;
use MiamiOH\WSPositionBudget\Repositories\MUIDRepositoryResourceCall;

class Service extends \MiamiOH\RESTng\Service
{
    protected $dataSource = 'MUWS_GEN_PROD';

    /**
     * @var Request
     */
    private $request = null;

    /**
     * @var Response
     */
    private $response = null;

    /**
     * @var User
     */
    protected $user = null;

    /**
     * @var JobLaborDistributionRepository
     */
    protected $repository = null;

    /**
     * @var JobLaborDistributionPersistence
     */
    protected $persistence = null;

    public function setDataSource(DataSource $dataSourceManager)
    {
        $dataSourceConfig = $dataSourceManager->getDataSource($this->dataSource);

        RESTngEloquentFactory::boot($dataSourceConfig);
    }

    public function getDependencies()
    {
        date_default_timezone_set('US/Eastern');

        $this->user = $this->getApiUser();
        $this->response = $this->getResponse();
        $this->request = $this->getRequest();

        $muidCall = function (array $params = [], array $options = []) {
            return $this->callResource('muid.get', $params, $options);
        };

        $muidRepository = new MUIDRepositoryResourceCall($muidCall);

        $this->repository = new JobLaborDistributionRepositorySQL($muidRepository);

        $this->persistence = new JobLaborDistributionPersistence($muidRepository);
    }

    /**
     * @throws \Exception
     */
    public function postSingle()
    {
        $this->getDependencies();

        $postService = new Post();

        $response = $postService->postSingle(
            $this->request,
            $this->response,
            $this->user,
            $this->repository
        );

        return $response;
    }

    /**
     * @throws \Exception
     */
    public function postCollection()
    {
        $this->getDependencies();

        $postService = new Post();

        $response = $postService->postCollection(
            $this->request,
            $this->response,
            $this->user,
            $this->persistence
        );

        return $response;
    }

    /**
     * @throws \Exception
     */
    public function putSingle()
    {
        $this->getDependencies();

        $putService = new Put();

        $response = $putService->putSingle(
            $this->request,
            $this->response,
            $this->user,
            $this->repository
        );

        return $response;
    }

    /**
     * @throws \Exception
     */
    public function putCollection()
    {
        $this->getDependencies();

        $putService = new Put();

        $response = $putService->putCollection(
            $this->request,
            $this->response,
            $this->user,
            $this->persistence
        );

        return $response;
    }

    /**
     * @throws \Exception
     */
    public function getJobLaborDistribution()
    {
        $this->getDependencies();

        $getService = new Get();

        $response = $getService->getJobLaborDistribution(
            $this->request,
            $this->response,
            $this->repository
        );

        return $response;
    }

}
