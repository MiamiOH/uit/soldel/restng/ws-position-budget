<?php

namespace MiamiOH\WSPositionBudget\Services\PositionLaborDistribution;

use MiamiOH\RESTng\Util\User;
use MiamiOH\WSPositionBudget\Objects\PositionLaborDistribution;
use MiamiOH\WSPositionBudget\Persistences\PositionLaborDistributionPersistence;
use MiamiOH\WSPositionBudget\Repositories\PositionLaborDistributionRepository;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory as Validator;
use MiamiOH\RESTng\App;
use MiamiOH\WSPositionBudget\Exceptions\ResourceExistsException;

class Post
{
    private static $bodyRules = [
        //required fields
        'percentage' => ['required', 'numeric'],
        'accountCode' => ['required', 'max:6'],
        'accountIndexCode' => ['required', 'max:6'],

        //non required fields
        'budget' => ['numeric'],
        'budgetIdCode' => ['max:6'],
        'budgetPhaseCode' => ['max:6'],
        'budgetToPost' => ['numeric'],
        'changeIndicator' => ['max:1'],
        'chartOfAccountsCode' => ['max:1'],
        'costTypeCode' => ['max:2'],
        'externalAccountCode' => ['max:60'],
        'projectCode' => ['max:8'],
    ];

    private static $paramRules = [
        'positionNumber' => ['required', 'max:6'],
        'fiscalYear' => ['required', 'numeric', 'regex:/^\d{4,4}$/'],
    ];

    /**
     * @param Request $request
     * @param Response $response
     * @param User $user
     * @param PositionLaborDistributionRepository $repository
     * @return Response
     * @throws \Exception
     */
    public function postSingle(
        Request $request,
        Response $response,
        User $user,
        PositionLaborDistributionRepository $repository
    ): Response {
        $status = \MiamiOH\RESTng\App::API_CREATED;

        $data = $request->getData();

        if (empty($data)) {
            $payload['errors'][] = 'No Data';
            $status = \MiamiOH\RESTng\App::API_BADREQUEST;

            $response->setPayload($payload);
            $response->setStatus($status);
            return $response;
        }

        $data['userId'] = strtoupper($user->getUsername());
        $data['dataOrigin'] = 'WebService';

        try {
            $positionLaborDistribution = PositionLaborDistribution::fromArray($data);
            $result = $repository->create($positionLaborDistribution);
        } catch (\Exception $e) {
            $payload['errors'][] = $e->getMessage();
            $response->setPayload($payload);
            if ($e instanceof \InvalidArgumentException) {
                $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);
            } else {
                $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
            }
            return $response;
        }

        // DONE
        $response->setPayload(['1 record created.']);
        $response->setStatus($status);
        return $response;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param User $user
     * @return Response
     * @throws \Exception
     */
    public function postCollection(
        Request $request,
        Response $response,
        User $user
    ): Response {
        $data = $request->getData();
        $positionNumber = $request->getResourceParam('positionNumber');
        $fiscalYear = intval($request->getResourceParam('fiscalYear'));

        $payload = [];

        if (empty($data)) {
            $payload['error']['message'] = 'No Data.';

            $response->setPayload($payload);
            $response->setStatus(App::API_BADREQUEST);
            return $response;
        }

        $errors = $this->validateInput($positionNumber, $fiscalYear, $data);

        if (!empty($errors)) {
            $payload['error']['message'] = implode(' ', $errors);

            $response->setPayload($payload);
            $response->setStatus(App::API_BADREQUEST);
            return $response;
        }

        try {
            $persistence = new PositionLaborDistributionPersistence();
            $persistence->create($positionNumber, $fiscalYear, $data, $user);
        } catch (\Exception $e) {
            $payload['error']['message'] = $e->getMessage();
            $response->setPayload($payload);

            if ($e instanceof ResourceExistsException) {
                $response->setStatus(App::API_ENTITYALREADYEXISTS);
            } elseif ($e instanceof \InvalidArgumentException) {
                $response->setStatus(App::API_BADREQUEST);
            } else {
                $response->setStatus(App::API_FAILED);
            }

            return $response;
        }

        // DONE
        $response->setPayload(['Records successfully created.']);
        $response->setStatus(App::API_CREATED);
        return $response;
    }

    private function validateInput(string $positionNumber, int $fiscalYear, array $data): array
    {
        $errors = [];
        $totalPercentage = 0;

        $validator = Validator::make(['positionNumber' => $positionNumber, 'fiscalYear' => $fiscalYear], self::$paramRules);

        if ($validator->fails()) {
            $errors[] = "Validation failed. " . implode(' ', $validator->errors()->all());
        }

        // Validate Input
        foreach ($data as $index => $row) {
            $rowNumber = $index + 1;
            $validator = Validator::make($row, self::$bodyRules);

            if ($validator->fails()) {
                $errors[] = "Row $rowNumber: Validation failed. " . implode(' ', $validator->errors()->all());
                continue;
            }

            $totalPercentage += floatval($row['percentage']);
        }

        if (empty($errors) && $totalPercentage != 100) {
            $errors[] = "Total percentage should be 100; provided total percentage is $totalPercentage.";
        }
        return $errors;
    }
}
