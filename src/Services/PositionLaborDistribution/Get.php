<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 4/16/19
 * Time: 11:55 AM
 */

namespace MiamiOH\WSPositionBudget\Services\PositionLaborDistribution;

use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;
use MiamiOH\WSPositionBudget\EloquentModels\PositionLaborDistributionEloquentModel;
use MiamiOH\WSPositionBudget\Repositories\PositionLaborDistributionRepositorySQL;

class Get
{
    private $rules = [
        'position' => ['required', 'max:6'],
        'fiscalYearCode' => ['required', 'max:4']
    ];
    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @throws \MiamiOH\RESTng\Exception\BadRequest
     */
    public function getPositionLaborDistribution(
        Request $request,
        Response $response
    )
    {
        $status = \MiamiOH\RESTng\App::API_OK;

        $options = $request->getOptions();

        $position = $options['position'] ?? '';

        $fiscalYear = $options['fiscalYearCode'] ?? '';

        //Validate input parameters
        $position = trim($position);
        $fiscalYear = trim($fiscalYear);

        $validator = RESTngValidatorFactory::make(
          [
              'position' => $position,
              'fiscalYearCode' => $fiscalYear
          ],
          $this->rules
        );

        if ($validator->fails()) {
            $response->setPayload([
                $validator->errors()->getMessages()
            ]);
            $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);

            return $response;
        }

        $laborDistributionSql = new PositionLaborDistributionRepositorySQL(new PositionLaborDistributionEloquentModel());

        if (empty($options['position'])){
            throw new \MiamiOH\RESTng\Exception\BadRequest('Please provide correct position');
        }

        if(empty($options['fiscalYearCode'])){
            throw new \MiamiOH\RESTng\Exception\BadRequest('Please provide correct fiscal year code');
        }

        $laborDistributionDetails = $laborDistributionSql->get($position, $fiscalYear);


        if (empty($laborDistributionDetails)) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }

        $response->setStatus($status);
        $response->setPayload($laborDistributionDetails);

        return $response;

    }

}
