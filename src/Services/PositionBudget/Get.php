<?php
/**
 * Created by PhpStorm.
 * User: kodebopp
 * Date: 04/16/19
 * Time: 4:40 PM
 */

namespace MiamiOH\WSPositionBudget\Services\PositionBudget;

use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;
use MiamiOH\WSPositionBudget\Repositories\PositionBudgetRepository;

/**
 * Class Get
 * @package MiamiOH\WSPositionBudget\Services\PositionBudget
 */
class Get
{
    private $rules = [
        'positionNumber' => ['required', 'max:6'],
        'fiscalYear' => ['required', 'max:4'],
    ];


    /**
     * @param Request $request
     * @param Response $response
     * @param PositionBudgetRepository $repository
     * @return Response
     * @throws \MiamiOH\RESTng\Exception\BadRequest
     */
    public function getSingle(
        Request $request,
        Response $response,
        PositionBudgetRepository $repository
    ) {
        $options = $request->getOptions();

        $positionNumber = $options['positionNumber'] ?? '';
        $fiscalYearCode = $options['fiscalYear'] ?? '';

        $positionNumber = trim($positionNumber);

        $validator = RESTngValidatorFactory::make(
            [
                'positionNumber' => $positionNumber,
                'fiscalYear' => $fiscalYearCode
            ],
            $this->rules
        );

        if ($validator->fails()) {
            $response->setPayload([
                $validator->errors()->getMessages()
            ]);
            $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);

            return $response;
        }

        $budgetDetails = $repository->get($options['positionNumber'], $options['fiscalYear']);

        if (empty($budgetDetails)) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($budgetDetails);

        return $response;
    }
}
