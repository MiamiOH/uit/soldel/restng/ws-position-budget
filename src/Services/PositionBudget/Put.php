<?php
/**
 * Created by PhpStorm.
 * User: kodebopp
 * Date: 04/16/19
 * Time: 4:40 PM
 */

namespace MiamiOH\WSPositionBudget\Services\PositionBudget;

use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use MiamiOH\WSPositionBudget\Objects\PositionBudget;
use MiamiOH\WSPositionBudget\Repositories\PositionBudgetRepository;
use function var_dump;

class Put
{
    /**
     * @param Request $request
     * @param Response $response
     * @param PositionBudgetRepository $repository
     * @param User $user
     * @return Response
     * @throws \Exception
     */
    public function putSingle(
        Request $request,
        Response $response,
        User $user,
        PositionBudgetRepository $repository
    ): Response {
        $positionNumber = $request->getResourceParam('positionNumber');
        $fiscalYearCode = $request->getResourceParam('fiscalYearCode');

        $data = $request->getData();

        # Make sure data is not empty
        if (empty($data)) {
            $payLoad['errors'][] = 'No Data';
            $status = \MiamiOH\RESTng\App::API_BADREQUEST;

            $response->setPayLoad($payLoad);
            $response->setStatus($status);
            return $response;
        }
        $data['userId'] = strtoupper($user->getUsername());
        $data['dataOrigin'] = 'WebService';
        $data['position'] = $positionNumber;
        $data['fiscalYearCode'] = $fiscalYearCode;

        try {
            $positionBudget = PositionBudget::fromPutArray($data);
            $result = $repository->update($positionBudget);
        } catch (\Exception $e) {
            $response->setPayload([$e->getMessage()]);
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
            return $response;
        }

        if (empty($result)) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }

        // DONE
        $response->setPayload(['1 record updated.']);
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        return $response;
    }
}
