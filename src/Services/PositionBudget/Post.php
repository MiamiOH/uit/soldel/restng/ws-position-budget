<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 12/4/18
 * Time: 4:40 PM
 */

namespace MiamiOH\WSPositionBudget\Services\PositionBudget;

use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use MiamiOH\WSPositionBudget\Objects\PositionBudget;
use MiamiOH\WSPositionBudget\Repositories\PositionBudgetRepository;

class Post
{
    /**
     * @param Request $request
     * @param Response $response
     * @param PositionBudgetRepository $repository
     * @param User $user
     * @return Response
     * @throws \Exception
     */
    public function postSingle(
        Request $request,
        Response $response,
        User $user,
        PositionBudgetRepository $repository
    ): Response
    {
        $status = \MiamiOH\RESTng\App::API_CREATED;

        $data = $request->getData();

        if (empty($data)) {
            $payload['errors'][] = 'No Data';
            $status = \MiamiOH\RESTng\App::API_BADREQUEST;

            $response->setPayload($payload);
            $response->setStatus($status);
            return $response;
        }

        $data['userId'] = strtoupper($user->getUsername());
        $data['dataOrigin'] = 'WEB SERVICE';

        try {
            $positionBudget = PositionBudget::fromPostArray($data);
            $result = $repository->create($positionBudget);
        } catch (\Exception $e) {
            $payload['errors'][] = $e->getMessage();
            $response->setPayload($payload);
            if ($e instanceof \InvalidArgumentException) {
                $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);
            } else {
                $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
            }
            return $response;
        }
        // DONE
        $response->setPayload(['1 record created.']);
        $response->setStatus($status);
        return $response;
    }
}
