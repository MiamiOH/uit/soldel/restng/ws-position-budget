<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 9/24/18
 * Time: 3:05 PM
 */

namespace MiamiOH\WSPositionBudget\Services\PositionBudget;

use MiamiOH\RESTng\Legacy\DataSource;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use MiamiOH\RESTngIlluminateIntegration\RESTngEloquentFactory;
use MiamiOH\WSPositionBudget\Repositories\PositionBudgetRepository;
use MiamiOH\WSPositionBudget\Repositories\PositionBudgetRepositorySQL;

class Service extends \MiamiOH\RESTng\Service
{
    /**
     * @var Request
     */
    private $request = null;

    /**
     * @var Response
     */
    private $response = null;

    /**
     * @var User
     */
    protected $user = null;

    /**
     * @var PositionBudgetRepository
     */
    protected $repository = null;

    public function getDependencies()
    {
        date_default_timezone_set('US/Eastern');

        $this->user = $this->getApiUser();
        $this->response = $this->getResponse();
        $this->request = $this->getRequest();

        $this->repository = new PositionBudgetRepositorySQL();
    }

    public function getSingle()
    {
        $this->getDependencies();

        $getService = new Get();

        return $getService->getSingle(
            $this->request,
            $this->response,
            $this->repository
        );
    }

    /**
     * @throws \Exception
     */
    public function postSingle()
    {
        $this->getDependencies();

        $postService = new Post();

        $response = $postService->postSingle(
            $this->request,
            $this->response,
            $this->user,
            $this->repository
        );
        return $response;
    }

    public function putSingle()
    {
        $this->getDependencies();

        $putService = new Put();

        $response = $putService->putSingle(
            $this->request,
            $this->response,
            $this->user,
            $this->repository
        );
        return $response;
    }
}
