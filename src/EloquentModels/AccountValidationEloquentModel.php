<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 12/1/18
 * Time: 8:39 PM
 */

namespace MiamiOH\WSPositionBudget\EloquentModels;

use Illuminate\Database\Eloquent\Model;
use Yajra\Oci8\Query\OracleBuilder;

class AccountValidationEloquentModel extends Model
{ /**
 * @var string $connection Connection
 */
    protected $connection = 'MUWS_GEN_PROD';
    /**
     * @var string $table Table name
     */
    public $table = 'ftvacct';

    /**
     * @var bool $timestamps Do not populate auto-generated date fields
     */
    public $timestamps = false;

    /**
     * @var bool $incrementing Do not increment primary key by default
     */
    public $incrementing = false;

    /**
     * @var string $primaryKey Primary key of table
     */
    protected $primaryKey = ['ftvacct_coas_code', 'ftvacct_acct_code', 'ftvacct_eff_date', 'ftvacct_nchg_date'];

    /**
     * @var array $guarded black list of insertable fields
     */
    protected $guarded = [];

    /**
     * Get a new query builder instance for the connection.
     * https://github.com/yajra/laravel-oci8/issues/73#issuecomment-117131744
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function newBaseQueryBuilder()
    {
        $conn = $this->getConnection();

        $grammar = $conn->getQueryGrammar();

        return new OracleBuilder($conn, $grammar, $conn->getPostProcessor());
    }
}
