<?php

return [
    'resources' => [
        'job' => [
            \MiamiOH\WSPositionBudget\Resources\JobLaborDistributionResourceProvider::class
        ],
        'position' => [
            \MiamiOH\WSPositionBudget\Resources\PositionBudgetResourceProvider::class,
            \MiamiOH\WSPositionBudget\Resources\PositionLaborDistributionResourceProvider::class,
        ]
    ]
];
